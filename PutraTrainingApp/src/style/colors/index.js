
const colors = {
    white: '#ffffff', 
    blue: '#3498db',
    black: '#000000',
    grey: '#bdc3c7',
    darkBlue: '#191970',
    transparent: 'rgba(0, 0, 0, 0.5)',
    lightGrey:'#ecf0f1'

};

export default colors;