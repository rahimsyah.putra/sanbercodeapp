import { StyleSheet } from 'react-native'

import colors from './colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.blue
    },
    quotesContainer: {
        flex: 1,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    quotes: {
        fontSize: 16,
        color: colors.white
    },
    logo: {
        alignSelf: 'center',
        justifyContent: 'center',
        marginLeft:20
    },
    textLogo: {
        fontSize: 30,
        fontWeight: 'bold',
        color: colors.white,
        alignSelf:'center'
        
    },
    textLogoContainer:{
        alignSelf:'center',
        padding:12,
        marginTop:20
    },
    listContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    listContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        maxWidth:250,
        maxHeight:250
    },
    imgList:{
        maxWidth:250,
        maxHeight:250
    },
    textList:{
        fontSize: 16,
        fontWeight: 'bold',
        color: colors.white,
        marginTop:10,
        textAlign:'center'
    },
    slider:{
        flex:1,
    },
    activeDotStyle:{
        backgroundColor:colors.white,
    },
    btnContainer:{
        flex:0.4,
        justifyContent:'center',
        backgroundColor:colors.blue,
    },
    btnLogin:{
        marginVertical:5,
        backgroundColor:colors.white,
        borderRadius:25,
        borderWidth:2,
        borderColor:colors.white,
        marginVertical:10,
        marginHorizontal:20,
        padding:10,
        elevation:5
    },
    btnTextLogin:{
        fontSize: 16,
        fontWeight: 'bold',
        textAlign:'center',
        color: colors.blue
    },
    btnRegister:{
        backgroundColor:colors.blue,
        borderRadius:25,
        borderWidth:2,
        borderColor:colors.white,
        marginVertical:10,
        marginHorizontal:20,
        padding:10,
        elevation:5
    },
    btnTextRegister:{
        fontSize: 16,
        fontWeight: 'bold',
        textAlign:'center',
        color: colors.white
    }
})

export default styles;