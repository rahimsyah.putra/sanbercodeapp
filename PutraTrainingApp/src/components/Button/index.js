import React from 'react'
import { TouchableOpacity } from 'react-native'

const Button = (title) =>{
    return(
        <TouchableOpacity {...title}/>
    )
}
export default Button ;