import React, { useState, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Icon from 'react-native-vector-icons/MaterialIcons';
import Intro from '../screens/Tugas6/Intro';
import SplashScreens from '../screens/Tugas6/SplashScreen';
import Home from '../screens/Tugas6/Home'
import Login from '../screens/Tugas6/Login';
import Register from '../screens/Tugas6/Register';
import Profile from '../screens/Tugas6/Profile';
import editProfile from '../screens/Tugas6/Profile/editProfile'
import Verification from '../screens/Tugas6/Register/verification'
import editPassword from '../screens/Tugas6/Register/editPassword'
import DaftarDonasi from '../screens/Tugas6/Donasi'
import Detail from '../screens/Tugas6/Detail'
import Payment from '../screens/Tugas6/Payment'
import Maps from '../screens/Tugas6/Maps'
import colors from '../style/colors';
import Statistics from '../screens/Tugas6/Statistics';
import Inbox from '../screens/Tugas6/Inbox'
import Riwayat from '../screens/Tugas6/Riwayat'
import Bantu from '../screens/Tugas6/Bantu'

const Stack = createStackNavigator()
const Tab = createMaterialBottomTabNavigator()

const MainNavigation = (props) => (
    <Stack.Navigator initialRouteName={props.token !== null ? 'Home' : 'Intro'}>
        <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Home" component={BottomTabNavigator} options={{ headerShown: false }} />
        <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
        <Stack.Screen name="Verification" component={Verification} options={{ headerShown: false }} />
        <Stack.Screen name="editPassword" component={editPassword} options={{ headerShown: false }} />
        <Stack.Screen name="editProfile" component={editProfile} options={{ headerShown: false }} />
        <Stack.Screen name="DaftarDonasi" component={DaftarDonasi} options={{ headerShown: false }} />
        <Stack.Screen name="Detail" component={Detail} options={{ headerShown: false }} />
        <Stack.Screen name="Payment" component={Payment} options={{ headerTintColor: '#fff', headerTitleStyle: { color: colors.white }, headerStyle: { backgroundColor: colors.blue, } }} />
        <Stack.Screen name="Bantuan" component={Maps} options={{ headerTintColor: '#fff', headerTitleStyle: { color: colors.white }, headerStyle: { backgroundColor: colors.blue, } }} />
        <Stack.Screen name="Statistics" component={Statistics} options={{ headerTintColor: '#fff', headerTitleStyle: { color: colors.white }, headerStyle: { backgroundColor: colors.blue, } }} />
        <Stack.Screen name="Riwayat Transaksi" component={Riwayat} options={{ headerTintColor: '#fff', headerTitleStyle: { color: colors.white }, headerStyle: { backgroundColor: colors.blue, } }} />
        <Stack.Screen name="Buat Donasi" component={Bantu} options={{ headerTintColor: '#fff', headerTitleStyle: { color: colors.white }, headerStyle: { backgroundColor: colors.blue, } }} />
    </Stack.Navigator>
)

const BottomTabNavigator = () => {
    return (
        <Tab.Navigator
            activeColor={colors.blue}
            inactiveColor={colors.blue}
            barStyle={{ backgroundColor: '#ffffff' }}>
            <Tab.Screen
                name="Home"
                component={Home}
                options={{
                    tabBarIcon: () => <Icon name="home" size={25} style={{ color: colors.blue }} />
                }} />
            <Tab.Screen
                name="Inbox"
                component={Inbox}
                options={{
                    tabBarIcon: () => <Icon name="inbox" size={25} style={{ color: colors.blue }} />
                }} />
            <Tab.Screen
                name="Profile"
                component={Profile}
                options={{
                    tabBarIcon: () => <Icon name="account-circle" size={25} style={{ color: colors.blue }} />
                }} />
        </Tab.Navigator >
    )
}


const AppNavigation = () => {

    const [isLoading, setIsLoading] = useState(true)
    const[token, setToken] = useState(null)

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)

        async function getToken() {
            const token = await AsyncStorage.getItem('token')
            setToken(token)
        }

        getToken()
    }, [])

    if (isLoading) {
        return <SplashScreens />
    }

    return (
        <NavigationContainer>
            <MainNavigation token={token}/>
        </NavigationContainer>
    )
}

export default AppNavigation