import React, { useContext } from 'react'
import {
    View,
    Text,
    StyleSheet,
    StatusBar,
    TouchableOpacity,
    TextInput,
    FlatList,
    SafeAreaView,
} from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';

import { RootContext } from '../Tugas4'

const TodoList4 = () => {
    return (
        <SafeAreaView style={styles.container}>
            <StatusBar barStyle="light-content" backgroundColor='#3498db' />
            <View >
                <TabBar />
                <TabForm />
            </View>
        </SafeAreaView>
    )
}

const TabBar = () => {
    return (
        <View style={styles.tabBar}>
            <Text style={styles.textBar}>
                Todo List
                </Text>
        </View>
    )
}

const TabForm = () => {

    //console.log("TodoList -> state", state)
    
    const Consumer = RootContext.Consumer

    return (
        <Consumer>
            {
                state => (
                    <View style={styles.containerForm}>
                        <View>
                            <Text style={styles.textJudul}>
                                Masukan Todolist
                            </Text>
                        </View>
                        <View style={styles.tabInput}>
                            <View style={styles.formInput}>
                                <TextInput
                                    style={styles.inputText}
                                    placeholder="Input text here"
                                    value={state.inputItem}
                                    onChangeText={(text) => state.handleChangeInput(text)}
                                />
                            </View>
                            <TouchableOpacity onPress={() => state.addNewTodo(state.inputItem)}>
                                <Icon style={styles.btn} name="add-outline" size={25} />
                            </TouchableOpacity>
                        </View>
                        
                        <FlatList
                            data={state.todos}
                            keyExtractor={item => item.text}
                            renderItem={({ item, index }) =>
                                <View style={styles.containerTodo}>
                                    <View style={styles.positionTodo}>
                                        <Text style={styles.textDate}>
                                            {item.date}
                                        </Text>
                                        <Text style={styles.inputText}>
                                            {item.text}
                                        </Text>
                                    </View>
                                    <TouchableOpacity onPress={() => state.removeTodo(index)}>
                                        <Icon style={styles.btnRemove} name="trash-outline" size={25} />
                                    </TouchableOpacity>
                                </View>
                            }
                        />

                    </View>
                )
            }
        </Consumer>
    )
}

export default TodoList4;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f5f5f5'
    },
    tabBar: {
        backgroundColor: '#3498db',
        padding: 15,
        marginBottom: 8
    },
    textBar: {
        alignSelf: 'center',
        fontWeight: 'bold',
        color: '#ffffff',
        fontSize: 20
    },
    containerForm: {
        flexDirection: 'column',
        padding: 2,
        backgroundColor: '#f5f5f5'
    },
    containerTodo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 10,
        marginVertical: 5,
        padding: 2,
        paddingVertical: 13,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#e0e0e0',
        elevation: 3,
        backgroundColor: '#ffffff'
    },
    positionTodo: {
        flexDirection: 'column',
        marginHorizontal: 10,
        backgroundColor: '#ffffff'
    },
    tabInput: {
        flexDirection: 'row',
        marginBottom: 10
    },
    formInput: {
        flex: 1,
        borderWidth: 1,
        borderRadius: 10,
        marginLeft: 7,
        backgroundColor: '#ffffff'
    },
    inputText: {
        marginLeft: 4,
        fontSize: 16
    },
    textDate: {
        marginBottom: 5,
        fontSize: 13,
        fontWeight: 'bold',
        color: '#3498db',
    },
    textJudul: {
        marginLeft: 7,
        marginBottom: 10,
        fontWeight: 'bold'
    },
    btn: {
        backgroundColor: '#3498db',
        borderRadius: 10,
        padding: 12,
        alignSelf: 'center',
        marginLeft: 5,
        marginRight: 8
    },
    btnRemove: {
        backgroundColor: '#ffffff',
        borderRadius: 10,
        padding: 12,
        marginRight: 5

    }
});
