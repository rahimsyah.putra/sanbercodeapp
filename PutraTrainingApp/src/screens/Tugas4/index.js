import React, {useState, createContext} from 'react'

import TodoList from './TodoList'

export const RootContext = createContext();

const context = () => {

    const [inputItem, setInput] = useState('')
    const [todos, setTodos] = useState([])

    const Provider = RootContext.Provider
    
    handleChangeInput = (text) => {
        setInput(text)
    }

    addNewTodo = () => {
        const date = new Date().getDate();
        const month = new Date().getMonth() + 1
        const year = new Date().getFullYear()

        const today = `${date}/${month}/${year}`

        setTodos([...todos, {text: inputItem, date: today}])
        setInput('')
    }

    removeTodo  = (id) => {
        setTodos(
            todos.filter((todo, index) => {
                if (index !== id) {
                    return true
                }
            })
        )
    }

    return(
        <Provider value={{
            inputItem,
            todos,
            handleChangeInput,
            addNewTodo,
            removeTodo
        }}>
            <TodoList/>
        </Provider>
    )
}

export default context;