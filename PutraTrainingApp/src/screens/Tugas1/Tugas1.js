import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

const tugas = () => {
    return (
        <View style={styles.container}>
            <Text> Hallo Kelas React Native Lanjutan Sanbercode! </Text>
        </View>
    )
}

export default tugas

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    }
})