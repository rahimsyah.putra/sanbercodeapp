import React, { useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    StatusBar,
    TouchableOpacity,
    TextInput,
    FlatList,
    SafeAreaView,
} from "react-native";

import Icon from 'react-native-vector-icons/Ionicons';

const tugas3 = () => {
    return (
        <SafeAreaView style={styles.container}>
            <StatusBar barStyle="light-content" backgroundColor='#3498db' />
            <View >
                <TabBar />
                <TabForm />
            </View>
        </SafeAreaView>
    )
}

const TabBar = () => {
    return (
        <View style={styles.tabBar}>
            <Text style={styles.textBar}>
                Todo List
                </Text>
        </View>
    )
}

const TabForm = () => {
    const [inputItem, setInput] = useState('');

    const [todos, setTodos] = useState([])

    const addNewTodo = () => {

        const date = new Date().getDate();
        const month = new Date().getMonth() + 1
        const year = new Date().getFullYear()

        const today = `${date}/${month}/${year}`

        if (inputItem.length > 0) {
            setTodos([...todos, { text: inputItem, date: today }])
            setInput('')
        }
        console.log(inputItem)
    }

    const removeTodo = (id) => {
        setTodos(
            todos.filter((todo, index) => {
                if (index !== id) {
                    return true
                }
            })
        )
    }
    
    return (
        <View style={styles.containerForm}>

            <View>
                <Text style={styles.textJudul}>
                    Masukan Todolist
                    </Text>
            </View>
            <View style={styles.tabInput}>
                <View style={styles.formInput}>
                    <TextInput
                        style={styles.inputText}
                        placeholder="Input here"
                        value={inputItem}
                        onChangeText={(text) => setInput(text)}
                    />
                </View>
                <TouchableOpacity onPress={() => addNewTodo(inputItem)}>
                    <Icon style={styles.btn} name="add-outline" size={25} />
                </TouchableOpacity>
            </View>

            <FlatList
                data={todos}
                keyExtractor={item => item.text}
                renderItem={({ item, index }) =>
                    <View style={styles.containerTodo}>
                        <View style={styles.positionTodo}>
                            <Text style={styles.textDate}>
                                {item.date}
                            </Text>
                            <Text style={styles.inputText}>
                                {item.text}
                            </Text>
                        </View>
                        <TouchableOpacity onPress={() => removeTodo(index)}>
                            <Icon style={styles.btnRemove} name="trash-outline" size={25} />
                        </TouchableOpacity>
                    </View>
                }
            />
        </View>
    )
}

export default tugas3;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    tabBar: {
        backgroundColor: '#3498db',
        padding: 15,
        marginBottom: 10
    },
    textBar: {
        alignSelf: 'center',
        fontWeight: 'bold',
        color: '#ffffff',
        fontSize: 20
    },
    containerForm: {
        flexDirection: 'column',
        padding: 2,
    },
    containerTodo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 10,
        marginVertical: 5,
        padding: 3,
        paddingVertical: 13,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#e0e0e0',
        elevation: 1,
        backgroundColor: '#ffffff'
    },
    positionTodo: {
        flexDirection: 'column',
        marginHorizontal: 10,
        marginVertical: 5,
        backgroundColor: '#ffffff'
    },
    tabInput: {
        flexDirection: 'row',
        marginBottom: 10
    },
    formInput: {
        flex: 1,
        borderWidth: 1,
        borderRadius: 10,
        marginLeft: 7
    },
    inputText: {
        marginLeft: 2,
        fontSize: 16
    },
    textDate: {
        marginLeft: 2,
        marginBottom: 2,
        fontWeight: 'bold',
        color: '#3498db'
    },
    textJudul: {
        marginLeft: 7,
        marginBottom: 5
    },
    btn: {
        backgroundColor: '#3498db',
        borderRadius: 10,
        padding: 12,
        alignSelf: 'center',
        marginLeft: 5
    },
    btnRemove: {
        backgroundColor: '#ffffff',
        borderRadius: 10,
        padding: 12,
        marginLeft: 215

    }
});
