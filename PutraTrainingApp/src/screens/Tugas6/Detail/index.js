import React, { useState } from 'react'
import { Text, View, ScrollView, StatusBar, Image, TouchableOpacity, Modal, ImageBackground } from 'react-native'
import styles from './style'
import colors from '../../../style/colors'
import Ionicon from 'react-native-vector-icons/Ionicons';
import api from '../../../api'
import Topup from '../TopUp'

const Detail = ({ navigation, route }) => {

    const { item } = route.params

    const [isVisible, setIsVisible] = useState(false)


    let images = `${api + item.photo}`
    let imageProfile = `${api + item.user.photo}`

    let rupiah = '';
    var nilai = item.donation.toString().split('').reverse().join('');
    for(var i = 0; i < nilai.length; i++) if(i%3 == 0) rupiah += nilai.substr(i,3)+'.';

    const renderDonation = () => {

        return (
            <Modal
                animationType="fade"
                transparent
                visible={isVisible}
                onRequestClose={() => setIsVisible(false)}
            >
                <View style={styles.modalContainer}>
                    <View style={styles.modal}>
                        <Topup navigation={navigation} id={item.id} />
                    </View>
                </View>
            </Modal>
        )
    }

    return (
        <ScrollView>
            <View style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor={colors.blue} />

                <View style={styles.pembatas}>
                    <View>
                        <ImageBackground source={{ uri: images }} style={{ width: "100%", height: 250 }}>
                            <View style={styles.btnBack}>
                                <TouchableOpacity onPress={() => navigation.navigate('DaftarDonasi')} activeOpacity={0.2}>
                                    <Ionicon name="chevron-back" size={25} color={colors.white} />
                                </TouchableOpacity>
                            </View>
                        </ImageBackground>
                    </View>
                    <View style={styles.descContainer}>
                        <Text style={styles.judulDonasi}>{item.title}</Text>
                        <Text style={styles.textDonasi}>Dana yang dibutuhkan Rp. {rupiah.split('',rupiah.length-1).reverse().join('')}</Text>
                        <Text style={styles.textSubJudul}>Deskripsi </Text>
                        <Text style={styles.textDonasi}>{item.description}</Text>
                        <TouchableOpacity
                            style={styles.btnDonasi}
                            onPress={() => setIsVisible(true)}>
                            <Text style={styles.textBtnDonasi}>Donasi Sekarang</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View>
                    <Text style={styles.infoPenggalang}>Informasi Penggalangan Dana</Text>
                    <View>
                        <View style={styles.penggalangContainer}>
                            <Text style={styles.textPenggalangDana}> Penggalang Dana</Text>
                            <View style={styles.profileContainer}>
                                <Image source={{ uri: imageProfile }} style={{ width: 40, height: 40, borderRadius: 20 }} />
                                <Text style={styles.textPenggalang}>{item.user.name}</Text>
                            </View>
                        </View>
                    </View>
                </View>

            </View>
            {renderDonation()}
        </ScrollView>
    )
}

export default Detail;