import { StyleSheet, } from 'react-native'

import colors from '../../../style/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    descContainer: {
        marginHorizontal: 15,
        marginVertical: 10
    },
    judulDonasi: {
        fontSize: 16,
        fontWeight: 'bold',
        marginVertical: 5
    },
    textSubJudul: {
        fontSize: 15,
        fontWeight: 'bold',
        marginTop: 10
    },
    textDonasi: {
        fontSize: 14,
        marginTop: 5
    },
    btnBack: {
        position: 'absolute',
        width: 40,
        height: 40,
        backgroundColor: colors.transparent,
        left: 5,
        top: 5,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        opacity:0.8
    },
    btnDonasi: {
        width: "95%",
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: colors.blue,
        borderRadius: 10,
        marginTop: 25,
        marginBottom: 10
    },
    textBtnDonasi: {
        justifyContent: 'center',
        textAlign: 'center',
        color: colors.white,
        fontSize: 18,
        padding: 15
    },
    pembatas: {
        borderBottomWidth: 5,
        borderBottomColor: colors.grey,

    },
    infoPenggalang: {
        fontSize: 15,
        fontWeight: "bold",
        marginHorizontal: 20,
        marginVertical: 20
    },
    penggalangContainer: {
        flexDirection: 'column',
        marginHorizontal: 15,
        padding: 15,
        borderRadius: 15,
        borderWidth:2,
        borderColor: colors.white,
        backgroundColor:colors.white,
        width:"91%",
        elevation:5,
        marginBottom: 50,
        alignSelf:'center'
    },
    profileContainer: {
        flexDirection: 'row',
        marginVertical: 10,
        marginHorizontal: 10,
    },
    textPenggalang: {
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: 16,
        fontWeight: 'bold',
        marginHorizontal: 15
    },
    textPenggalangDana:{
        fontSize:15,
        marginBottom:5
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: colors.transparent
    },
    modal:{
        height: 310,
        padding: 20,
        borderTopRightRadius: 12,
        borderTopLeftRadius: 12,
        backgroundColor: colors.white,
    }
})

export default styles;