import React from 'react'
import { WebView } from 'react-native-webview'
import styles from './style'

const Payment =({ route }) => {
    return (
        <WebView
            style={styles.container}
            source={{ uri: route.params.midtrans.redirect_url }}
        />
    )
}

export default Payment;