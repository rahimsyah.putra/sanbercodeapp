import React, { useState } from 'react'
import { View, Text, StatusBar, TouchableOpacity, TextInput, Image } from 'react-native'
import styles from './style';
import colors from '../../../style/colors'
import Ionicon from 'react-native-vector-icons/Ionicons';
import Axios from 'axios'
import api from '../../../api';

const editPassword = ({ navigation, route }) => {

    const { item } = route.params;
    console.log("editpass -> data berhasil dipindahkan", item)

    const email = item.email

    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [isVisible, setIsVisible] = useState(true)

    const password_confirmation = confirmPassword

    const hidePassword = () => {
        setIsVisible(!isVisible)
    }

    const checkPassword = () => {
        if (password !== confirmPassword) {
            alert(" Password do not match")
        } else if (password == confirmPassword) {
            onSavePress()
        }
    }

    const onSavePress = () => {
        let data = {
            email,
            password,
            password_confirmation
        }
        Axios.post(`${api}/api/auth/update-password`, data, {
            timeout: 20000,  
        })
        .then((res) => {
            //console.log("verif -> res", res)
            alert("Berhasil mendaftarkan akun")
            navigation.navigate('Login')
        })
        .catch((err) => {
            console.log("verif -> err ", err)
            alert("Gagal mendaftarkan akun")
        })
        
    }

    return (
        < >
            <StatusBar barStyle="light-content" backgroundColor='#3498db' />
            <View style={styles.container}>
                <View style={styles.tabBar}>
                    <TouchableOpacity
                        onPress={() => navigation.navigate('Verification')}>
                        <Ionicon style={styles.item} name="chevron-back" size={25} color={colors.white} />
                    </TouchableOpacity>
                    <View style={styles.textLogoContainer}>
                        <Text style={styles.textLogo}>Ubah Kata Sandi</Text>
                    </View>
                </View>
                <View style={styles.containerForm}>
                    <Text style={styles.textSubJudul}>Password</Text>
                    <View style={styles.tabInput}>
                        <View style={styles.formInput}>
                            <TextInput
                                style={styles.inputText}
                                //underlineColorAndroid="#c6c6c6"
                                placeholder="Password"
                                value={password}
                                onChangeText={(password) => setPassword(password)}
                                secureTextEntry={isVisible}
                            />
                            <Ionicon name="eye-off-outline" size={25} color={colors.grey} style={styles.icon} onPress={() => hidePassword()} />
                        </View>
                    </View>
                    <Text style={styles.textSubJudul}>Password Confirmation</Text>
                    <View style={styles.tabInput}>
                        <View style={styles.formInput}>
                            <TextInput
                                style={styles.inputText}
                                //underlineColorAndroid="#c6c6c6"
                                placeholder="Password Confimation"
                                value={confirmPassword}
                                onChangeText={(confirmPassword) => setConfirmPassword(confirmPassword)}
                                secureTextEntry={isVisible}
                            />
                            <Ionicon name="eye-off-outline" size={25} color={colors.grey} style={styles.icon} onPress={() => hidePassword()} />
                        </View>
                    </View>

                    <TouchableOpacity
                        onPress={() => checkPassword()}
                        style={styles.btn}
                    >
                        <Text style={styles.textBtnDaftar}>SIMPAN</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </>
    )
}


export default editPassword;