import React, { useEffect, useState } from 'react'
import { View, Text, StatusBar, TouchableOpacity, TextInput, Image } from 'react-native'
import styles from './style';
import colors from '../../../style/colors'
import Ionicon from 'react-native-vector-icons/Ionicons';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import Axios from 'axios'
import api from '../../../api';

const Verification = ({ navigation, route }) => {

    const { item } = route.params;
    console.log("verif -> data berhasil dipindahkan", item)

    const email = item.email
    const [otp, setOtp] = useState('')

    const onVerifyPress = () => {
        let data = {
            otp
        }
        Axios.post(`${api}/api/auth/verification`, data, {
            timeout: 20000,  
        })
        .then((res) => {
            console.log("verif -> res", res)
            navigation.navigate('editPassword', {
                item: data,
                item: item
            })
            alert("Berhasil diverifikasi")
        })
        .catch((err) => {
            console.log("verif -> err ", err)
            alert("Gagal verifikasi, silahkan cek kembali nomor OTP")
        })
        
    }

    return (
        < >
            <StatusBar barStyle="light-content" backgroundColor='#3498db' />
            <View style={styles.container}>
                <View style={styles.tabBar}>
                    <TouchableOpacity
                        onPress={() => navigation.navigate('Register')}>
                        <Ionicon style={styles.item} name="chevron-back" size={25} color={colors.white} />
                    </TouchableOpacity>
                    <View style={styles.textLogoContainer}>
                        <Text style={styles.textLogo}>Daftar</Text>
                    </View>
                </View>
                <View>
                    <Text style={styles.textJudulDeskripsi}>Perjalanan Kebaikanmu dimulai dari sini</Text>
                    <Text style={styles.textDeskripsi}>Masukkan 6 digit kode yang kami kirimkan ke</Text>
                    <Text style={styles.textEmail}>{email}</Text>
                </View>
                <OTPInputView
                    style={{ width: '80%', height: 200, marginHorizontal: 40 }}
                    pinCount={6}
                    autoFocusOnLoad
                    codeInputFieldStyle={styles.underlineStyleBase}
                    codeInputHighlightStyle={styles.underlineStyleHighLighted}
                    onCodeFilled={(code) => setOtp(code)}
                />
                <TouchableOpacity
                    onPress={() => onVerifyPress()}
                    style={styles.btn}
                >
                    <Text style={styles.textBtnDaftar}>VERIFIKASI</Text>
                </TouchableOpacity>


                <View style={styles.containerReOtp}>
                    <Text style={styles.textReOtp} >Belum menerima kode verifikasi?</Text>
                    <TouchableOpacity
                        onPress={() => navigation.navigate('Login')}
                        style={styles.btnReOtp}
                    >
                        <Text style={styles.textReOtp} style={{ fontWeight: 'bold', color: colors.blue }}>Kirim Ulang</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </>
    )
}

export default Verification;