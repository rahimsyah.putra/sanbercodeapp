import { StyleSheet } from 'react-native'

import colors from '../../../style/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    tabBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: colors.blue,
        paddingVertical: 15,
    },
    textLogo: {
        fontSize: 24,
        fontWeight: 'bold',
        color: colors.white,
        marginLeft: 30
    },
    textLogoContainer: {
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    item: {
        justifyContent: 'center',
        alignSelf: 'center',
        marginLeft: 10,
        marginTop: 5,
        color: colors.white
    },
    logoLogin: {
        alignSelf: 'center',
        justifyContent:'center',
        width:"60%",
        height:120,
        borderRadius:10,
        marginVertical:"10%"
    },
    containerForm: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        padding: 2,
        marginTop:30,
        backgroundColor: '#ffffff'
    },
    tabInput: {
        flexDirection: 'row',
        marginVertical: 5,
        marginBottom: 10
    },
    formInput: {
        flex: 1,
        flexDirection:'row',
        marginLeft: 6,
        marginRight: 6,
        backgroundColor: '#ffffff',
        justifyContent:'space-between',
        borderBottomColor: '#bdc3c7',
        borderBottomWidth: 1,
    },
    inputText: {
        flex:1,
        marginLeft: 4,
        fontSize: 20,
    },
    icon:{
        alignSelf:'center',
        justifyContent:'center'  
    },
    textSubJudul: {
        marginTop: 5,
        marginLeft: 12,
        fontSize: 15,
        color: colors.black
    },
    btn: {
        backgroundColor: colors.blue,
        borderRadius: 15,
        padding: 12,
        alignSelf: 'center',
        paddingHorizontal: 125,
        marginTop: 10,
        marginLeft: 5,
        marginRight: 8
    },
    textBtnDaftar: {
        fontSize: 20,
        fontWeight: 'bold',
        color: colors.white,
    },
    textBtn: {
        fontSize: 15,
        fontWeight: 'bold',
        color: colors.black,
        marginVertical: 10
    },
    logoBtn: {
        width: 40,
        height: 40,
        marginRight: 15
    },
    logoBtnFb: {
        width: 20,
        height: 40,
        marginRight: 20
    },
    containerCreateAcc: {
        marginTop: 20,
        flexDirection: 'row',
        alignSelf: 'center',
    },
    btnCreateAcc: {
        backgroundColor: colors.white,
        alignSelf: 'center',
        paddingHorizontal: 10,
        marginTop: 5
    },
    textCreateAcc: {
        textAlign: 'center',
        marginTop: 5,
        fontSize: 15,
        color: colors.black
    },
    textOr: {
        textAlign: 'center',
        marginTop: 50,
        fontSize: 16,
        color: colors.black,
        fontWeight: 'bold'
    },
    btnContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.white,
    },
    btnGoogle: {
        flexDirection: 'row',
        backgroundColor: colors.white,
        borderWidth: 1,
        borderRadius: 15,
        elevation: 3,
        borderColor: colors.grey,
        padding: 12,
        alignSelf: 'center',
        paddingHorizontal: 20,
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
    },
    btnFacebook: {
        flexDirection: 'row',
        backgroundColor: colors.white,
        borderWidth: 1,
        borderRadius: 15,
        elevation: 3,
        borderColor: colors.grey,
        padding: 12,
        alignSelf: 'center',
        paddingHorizontal: 15,
        marginTop: 10,
        marginLeft: 5,
        marginRight: 8
    },
    borderStyleBase: {
        width: 40,
        height: 45
    },
    borderStyleHighLighted: {
        borderColor: "#03DAC6",
    },
    underlineStyleBase: {
        width: 35,
        height: 50,
        borderWidth: 0,
        borderBottomWidth: 1,
    },
    underlineStyleHighLighted: {
        borderColor: "#03DAC6",
    },

    containerReOtp: {
        marginTop: 20,
        flexDirection: 'column',
        alignSelf: 'center',
    },
    btnReOtp: {
        backgroundColor: colors.white,
        alignSelf: 'center',
        paddingHorizontal: 10,
        marginTop: 5
    },
    textReOtp: {
        textAlign: 'center',
        marginTop: 5,
        fontSize: 15,
        color: colors.black
    },
    textJudulDeskripsi:{
        marginTop:30,
        marginLeft:20,
        fontSize:18,
        fontWeight:'bold'
    },
    textDeskripsi:{
        marginTop:5,
        marginLeft:20,
        fontSize:15,
    },
    textEmail:{
        marginTop:5,
        marginLeft:20,
        fontSize:17,
        fontWeight:'bold'
    }
})

export default styles;