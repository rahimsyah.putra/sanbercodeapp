import React, {useState} from 'react'
import { View, Text, StatusBar, TouchableOpacity, TextInput, Image } from 'react-native'
import styles from './style';
import colors from '../../../style/colors'
import Ionicon from 'react-native-vector-icons/Ionicons';
import Axios from 'axios'
import api from '../../../api';

const Register = ({ navigation }) => {
    
    const [email, setEmail] = useState('')
    const [name, setName] = useState('')

    const onRegisterPress = () => {
        let data = {
            name: name,
            email: email
        }
        Axios.post(`${api}/api/auth/register`, data, {
            timeout: 20000,  
        })
        .then((res) => {
            console.log("register -> res", res)
            navigation.navigate('Verification', {
                item: data
            })
        })
        .catch((err) => {
            console.log("register -> err ", err)
            alert("Email sudah terdaftar")
        })
    }
    return (
        < >
            <StatusBar barStyle="light-content" backgroundColor='#3498db' />
            <View style={styles.container}>
                <TabBar />
                <View style={styles.containerForm}>
                    <Image source={require('../../../assets/images/logoLogin.png')} style={styles.logoLogin} />
                    <Text style={styles.textSubJudul}>Nomor Ponsel or Email</Text>
                    <View style={styles.tabInput}>
                        <View style={styles.formInput}>
                            <TextInput
                                style={styles.inputText}
                                placeholder="Phone Number or Email"
                                value={email}
                                onChangeText={(email) => setEmail(email)}
                            />
                        </View>
                    </View>
                    <Text style={styles.textSubJudul}>Nama Lengkap</Text>
                    <View style={styles.tabInput}>
                        <View style={styles.formInput}>
                            <TextInput
                                style={styles.inputText}
                                placeholder="Name"
                                value={name}
                                onChangeText={(name) => setName(name)}
                            />
                        </View>
                    </View>

                    <TouchableOpacity
                        onPress={() => onRegisterPress(name, email)}
                        style={styles.btn}
                    >
                        <Text style={styles.textBtnDaftar}>DAFTAR</Text>
                    </TouchableOpacity>
                    <View style={styles.containerCreateAcc}>
                        <Text style={styles.textCreateAcc} >Sudah punya akun?</Text>
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Login')}
                            style={styles.btnCreateAcc}
                        >
                            <Text style={styles.textCreateAcc} style={{ color: colors.blue }}>Masuk</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.textOr} >Atau lebih cepat dengan</Text>
                    <View style={styles.btnContainer}>
                        <TouchableOpacity
                            //onPress={()=> signInWithFingerprint()}
                            style={styles.btnFacebook}
                        >
                            <Image source={require('../../../assets/images/facebook.png')} style={styles.logoBtnFb}/>
                            <Text style={styles.textBtn}>Facebook</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            //onPress={()=> signInWithGoogle()}
                            style={styles.btnGoogle}
                        >
                            <Image source={require('../../../assets/images/google.png')} style={styles.logoBtn} />
                            <Text style={styles.textBtn}>Sign In</Text>
                        </TouchableOpacity>

                    </View>
                </View>
            </View>
        </>
    )
}

const TabBar = () => {
    return (
        <View style={styles.tabBar}>
            <TouchableOpacity
                onPress={() => navigation.navigate('Profile')}>
                <Ionicon style={styles.item} name="chevron-back" size={25} color={colors.white} />
            </TouchableOpacity>
            <View style={styles.textLogoContainer}>
                <Text style={styles.textLogo}>Daftar</Text>
            </View>
        </View>
    )
}


export default Register;