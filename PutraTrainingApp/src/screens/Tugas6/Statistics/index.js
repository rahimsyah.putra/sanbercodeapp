import React, { useState } from 'react'
import {  View, processColor } from 'react-native';
import styles from './style'
import { BarChart } from 'react-native-charts-wrapper'
import colors from '../../../style/colors';

const data = [
    { y: 100 },
    { y: 60 },
    { y: 90 },
    { y: 45 },
    { y: 67 },
    { y: 32 },
    { y: 150 },
    { y: 70 },
    { y: 40 },
    { y: 89 },
]

const month = [
    'Jan',
    'Feb', 
    'Mar', 
    'Apr',
    'Mei',
    'Jun',
    'Jul', 
    'Agu',
    'Sep', 
    'Oct', 
    'Nov', 
    'Des'
]

const Statistics = () => {

    const [legend, setLegend] = useState({
        enabled: false,
        textSize:14,
        form:'SQUARE',
        formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        formToTextSpace: 5,
        wordWrapEnabled: true,
        maxSizePercent: 0.5
    })
    const [chart, setChart] = useState({
        data: {
            dataSets: [{
                values: data,
                label: '',
                config: {
                    colors : [processColor(colors.blue)],
                    stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
                    drawFilled: false,
                    drawValues: false,
                }
            }]
        }
    })
    const [xAxis, setXAxis] = useState({
        valueFormatter: month,
        position: 'BOTTOM',
        drawAxisLine: true,
        drawGridLines: false,
        axisMinimum: -0.5,
        granularityEnabled: true,
        granularity: 1,
        axisMaximum: new Date().getMonth() + 0.5,
        spaceBetweenLabels: 0,
        labelRotationAngle: -45.0,
        limitLines: [{limit: 115, lineColor: processColor('red'), lineWidth: 1 }]
    })
    const [yAxis, setYAxis] = useState({
        left :{
            axisMinimum: 0,
            labelCountForce: true,
            granularityEnabled: true,
            granularity: 5,
            drawGridLines: false,
        },
        right:{
            axisMinimum: 0,
            labelCountForce: true,
            granularityEnabled: true,
            granularity: 5,
            enabled: false
        }
    })

    return (
        <View style={styles.container}>
            <BarChart 
                style={styles.chart}
                data={chart.data}
                legend={legend}
                pinchZoom={false}
                doubleTapToZoomEnabled={false}
                chartDescription={{ text: '' }}
                yAxis={yAxis}
                xAxis={xAxis}
                marker={{
                    enabled: true,
                    markerColor: processColor(colors.grey),
                    textColor: processColor(colors.white),
                    markerfontSize:14,
                    digits: 0,
                }}
            />
        </View>
    )
}

export default Statistics;