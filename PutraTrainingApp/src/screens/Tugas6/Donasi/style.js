import { StyleSheet, } from 'react-native'

import colors from '../../../style/colors'

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:colors.white
    },
    containerHeader:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingVertical:10,
        backgroundColor:colors.blue
    },
    textLogo: {
        fontSize: 20,
        fontWeight: 'bold',
        color: colors.white,
        marginLeft:15
    },
    textLogoContainer:{
        flex:1,
        alignSelf:'center',
        
    },
    item:{
        alignSelf:'center',
        marginLeft:10,
        color:colors.white
    },
    poster: {
        width: 170,
        height: 150,
        marginRight: 10,
        borderRadius:8
    },
    judulDonasi:{
        fontSize:16,
        fontWeight:'bold',
        width:"50%",
        marginVertical:5
    },
    textDonasi:{
        fontSize:14,
    }
})

export default styles;