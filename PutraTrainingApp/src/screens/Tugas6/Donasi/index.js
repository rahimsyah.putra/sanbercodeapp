import React, { useEffect, useState } from 'react'
import { Text, View, Image, StatusBar, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native'
import styles from './style'
import colors from '../../../style/colors'
import Ionicon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import api from '../../../api'
import { ProgressBar } from 'react-native-paper'

const DaftarDonasi = ({ navigation }) => {

    const [data, setData] = useState([])
    const [isLoading, setIsLoading] = useState(false)

    useEffect(() => {
        const getToken = async () => {
            try {
                const value = await AsyncStorage.getItem('token')
                if (value !== null) {
                    console.log("getToken ->", value)
                    return getDonation(value)
                }
            } catch (e) {
                console.log(e)
            }
        }
        getToken()
        getDonation()
    }, [])

    const getDonation = (token) => {
        setIsLoading(true)
        Axios.get(`${api}/api/donasi/daftar-donasi`, {
            timeout: 20000,
            headers: {
                "Authorization": "Bearer" + token,
            }
        })
            .then((res) => {
                setIsLoading(false)
                const dataSmt = res
                console.log("getDonation -> data", dataSmt)
                setData(dataSmt.data.data.donasi)

            })
            .catch((err) => {
                console.log("getDonation -> err", err)
            })
    }

    const onDonationPress = (item) => {
        navigation.navigate('Detail', {
            item: item
        })
    }

    if(isLoading){
        return <ActivityIndicator size='large' color={colors.blue}/>
    }

    const RenderItem = ({ item }) => {
        let images = `${api + item.photo}`

        let name = item.user.name

        let rupiah = '';
        var nilai = item.donation.toString().split('').reverse().join('');
        for(var i = 0; i < nilai.length; i++) if(i%3 == 0) rupiah += nilai.substr(i,3)+'.';

        return (
            <TouchableOpacity onPress={()=> onDonationPress(item)}>
                <View style={{ flexDirection: 'row', padding:10 , width:"100%", justifyContent:'space-between'}}>
                    <Image source={{ uri: images }} style={styles.poster} />
                     <View style={{ flexDirection: 'column' , width:"100%" }}>
                        <Text style={styles.judulDonasi}>{item.title}</Text>
                        <Text style={styles.textDonasi}>{name}</Text>
                        <ProgressBar progress={0.2} style={{ marginVertical: 10 , width:"45%"}} />
                        <Text style={styles.textDonasi}>Dana yang dibutuhkan:</Text>
                        <Text style={{fontWeight:'bold'}}>Rp. {rupiah.split('',rupiah.length-1).reverse().join('')}</Text>
                    </View>  
                </View>

            </TouchableOpacity>
        )
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
            <View style={styles.containerHeader}>
                <TouchableOpacity
                    onPress={() => navigation.navigate('Home')}>
                    <Ionicon style={styles.item} name="chevron-back" size={25} />
                </TouchableOpacity>
                <View style={styles.textLogoContainer}>
                    <Text style={styles.textLogo}>Donasi</Text>
                </View>
            </View>

            <FlatList
                data={data.slice(2,10)}
                style={{maxWidth:400}}
                maxToRenderPerBatch={8}
                keyExtractor={(item) => item.id}
                renderItem={RenderItem}
            />
            
        </View>
    )
}

export default DaftarDonasi;