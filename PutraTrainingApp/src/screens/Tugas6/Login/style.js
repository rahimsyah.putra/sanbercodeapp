import {StyleSheet} from 'react-native'
import colors from '../../../style/colors'

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:colors.white
    },
    containerLogin:{
        backgroundColor:colors.blue
    },
    logo: {
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    logoLogin:{
        alignSelf: 'center',
        justifyContent:'center',
        width:"60%",
        height:120,
        borderRadius:10,
        marginVertical:"10%"
    },
    textLogo: {
        fontSize: 26,
        fontWeight: 'bold',
        color: colors.white,
        alignSelf:'center',
        textAlign:'center',
        marginRight:40,
    },
    textLogoContainer:{
        flex:1,
        alignSelf:'center',
        
    },
    containerHeader:{
        flexDirection:'row',
        marginVertical:12,
    },
    icon:{
        alignSelf:'center',
        justifyContent:'center'  
    },
    item:{
        alignSelf:'center',
        marginLeft:10,
        marginTop:5,
        color:colors.white
    },
    containerForm: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        padding: 2,
        backgroundColor: '#ffffff'
    },
    tabInput: {
        flexDirection: 'row',
        marginVertical:5,
        marginBottom: 10
    },
    formInput: {
        flex: 1,
        flexDirection:'row',
        justifyContent:'space-between',
        marginLeft: 6,
        marginRight: 6,
        backgroundColor: colors.white,
        borderBottomColor: '#bdc3c7',
        borderBottomWidth: 1,
    },
    inputText: {
        marginLeft: 4,
        fontSize: 20
    },
    textSubJudul:{
        marginTop:5,
        marginLeft:12,
        fontSize: 15,
        color:colors.black
    },
    btn:{
        backgroundColor: colors.blue,
        borderRadius: 15,
        padding: 12,
        alignSelf: 'center',
        paddingHorizontal:125,
        marginTop:10,
        marginLeft: 5,
        marginRight: 8,
        elevation:3
    },
    btnGoogle:{
        backgroundColor: colors.blue,
        borderRadius: 15,
        padding: 12,
        alignSelf: 'center',
        paddingHorizontal:60,
        marginTop:10,
        marginLeft: 5,
        marginRight: 8,
        elevation:3
    },
    btnFingerprint:{
        backgroundColor: '#191970',
        borderRadius: 15,
        padding: 12,
        alignSelf: 'center',
        paddingHorizontal:30,
        marginTop:20,
        marginLeft: 5,
        marginRight: 8,
        elevation:3
    },
    textBtn:{
        fontSize:20,
        fontWeight:'bold',
        color:colors.white
    },
    textOr:{
        textAlign:'center',
        marginTop:5,
        fontSize:20,
        color:colors.black
    },
    containerCreateAcc:{
        marginTop:40,
        flexDirection:'row',
        alignSelf:'center'
    },
    btnCreateAcc:{
        backgroundColor: colors.white,
        alignSelf: 'center',
        paddingHorizontal:10,
        marginTop:5
    },
    textCreateAcc:{
        textAlign:'center',
        marginTop:5,
        fontSize:15,
        color:colors.black
    },
    
})

export default styles