import React, { useEffect, useState } from 'react'
import { View, Text, StatusBar, SafeAreaView, TextInput, TouchableOpacity, Image, Alert } from 'react-native'
import styles from './style';
import colors from '../../../style/colors'
import Ionicon from 'react-native-vector-icons/Ionicons';
import Axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import api from '../../../api';
import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-community/google-signin';
import TouchID from 'react-native-touch-id';

const config = {
    title: "Autentication Required",
    imageColor: '#191970',
    imageErrorColor: 'red',
    sensorDescription: 'Touch Sensor',
    sensorErrorDescription: 'Failed',
    cancelText: 'Cancel'
}

const Login = ({ navigation }) => {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isVisible, setIsVisible] = useState(true)

    const saveToken = async (res) => {
        try {
            if (res !== null) {
                await AsyncStorage.setItem("token", res)
            }
            //console.log("Token berhasil disave-> res", res)

        } catch (err) {
            console.log("Token tidak berhasil disave -> err", err)
        }
    }

    useEffect(()=> {
        configureGoogleSignIn()
    },[])

    const hidePassword = () => {
        setIsVisible(!isVisible)
    }

    const configureGoogleSignIn = () => {
        GoogleSignin.configure({
            webClientId: '398038654110-fr3hg2a66lm8rgg8ec0kqtg5mupmctbn.apps.googleusercontent.com'
        })
    }

    const signInWithGoogle = async () => {
        try {
            const { idToken } = await GoogleSignin.signIn()
            const credential = auth.GoogleAuthProvider.credential(idToken)
            
            auth().signInWithCredential(credential)
            //console.log("SignInWithGoogle -> idToken", idToken)
            navigation.reset({
                index: 0,
                routes: [{ name: 'Home' }]
            })
        } catch (error) {
            console.log("SignInWithGoogle -> error", error)
            alert("Authentication Failed")
        }
    }

    const signInWithFingerprint = () => {
        TouchID.authenticate('', config)
        .then(success => {
            alert("Authetication Succes")
            navigation.reset({
                index: 0,
                routes: [{ name: 'Home' }]
            })
        })
        .catch(error => {
            alert("Authentication Failed")
        })
    }

    const onLoginPress = () => {
        let data = {
            email: email,
            password: password
        }
        Axios.post(`${api}/api/auth/login`, data, {
            timeout: 20000,
        })
            .then((res) => {
                saveToken(res.data.data.token)
                //console.log("Token di dapatkan -> ", res.data.data.token)
                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Home' }]
                })
            })
            .catch((err) => {
                Alert.alert("Error", err.message)
            })
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.containerLogin}>
                <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
                <View style={styles.containerHeader}>
                    <TouchableOpacity
                        onPress={()=> navigation.navigate('Intro')}>
                        <Ionicon style={styles.item} name="chevron-back" size={25}/>
                    </TouchableOpacity>
                    <View style={styles.textLogoContainer}>
                        <Text style={styles.textLogo}>Login</Text>
                    </View>

                </View>
                <View style={styles.containerForm}>
                    <Image source={require('../../../assets/images/logoLogin.png')} style={styles.logoLogin} />
                    <Text style={styles.textSubJudul}>Username</Text>
                    <View style={styles.tabInput}>
                        <View style={styles.formInput}>
                            <TextInput
                                style={styles.inputText}
                                placeholder="Email"
                                value={email}
                                onChangeText={(email) => setEmail(email)}
                            />
                        </View>
                    </View>
                    <Text style={styles.textSubJudul}>Password</Text>
                    <View style={styles.tabInput}>
                        <View style={styles.formInput}>
                            <TextInput
                                style={styles.inputText}
                                placeholder="Password"
                                value={password}
                                onChangeText={(password) => setPassword(password)}
                                secureTextEntry={isVisible}
                            />
                            <Ionicon name="eye-off-outline" size={25} color={colors.grey} style={styles.icon} onPress={() => hidePassword()} />
                        </View>
                    </View>

                    <TouchableOpacity
                        onPress={() => onLoginPress()}
                        style={styles.btn}
                    >
                        <Text style={styles.textBtn}>LOGIN</Text>
                    </TouchableOpacity>
                    <Text style={styles.textOr} >OR</Text>
                    <TouchableOpacity
                        onPress={()=> signInWithGoogle()}
                        style={styles.btnGoogle}
                    >
                        <Text style={styles.textBtn}>LOGIN WITH GOOGLE</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={()=> signInWithFingerprint()}
                        style={styles.btnFingerprint}
                    >
                        <Text style={styles.textBtn}>SIGN IN WITH FINGERPRINT</Text>
                    </TouchableOpacity>
                    <View style={styles.containerCreateAcc}>
                        <Text style={styles.textCreateAcc} >Belum punya akun?</Text>
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Register')}
                            style={styles.btnCreateAcc}
                        >
                            <Text style={styles.textCreateAcc} style={{ color: colors.blue }}>Daftar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </SafeAreaView>
    )
}

export default Login;