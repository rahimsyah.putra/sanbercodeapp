import React, { useEffect, useRef, useState } from 'react'
import { View, Text, FlatList, TouchableOpacity } from 'react-native'
import styles from './style'
import Axios from 'axios'
import api from '../../../api'
import { TextInputMask } from 'react-native-masked-text'
import AsyncStorage from '@react-native-async-storage/async-storage'
import colors from '../../../style/colors'

const topup_list = [
    {
        id: 'topup10k',
        total: 10000
    },
    {
        id: 'topup20k',
        total: 20000
    },
    {
        id: 'topup50k',
        total: 50000
    },
    {
        id: 'topup100k',
        total: 100000
    },
    {
        id: 'topup200k',
        total: 200000
    },
    {
        id: 'topup500k',
        total: 500000
    },
]

const Topup = (item) => {
    
    console.log(item.id)

    let moneyField = useRef(null)
    const [data, setData] = useState([])
    const [total, setTotal] = useState('')
    const [token, setToken] = useState(null)
    const [selected, setSelected] = useState(null)

    useEffect(()=> {
        const getToken = async () => {
            try {
                const token = await AsyncStorage.getItem('token')
                if (token !== null){
                    setToken(token)
                    return getProfile(token)
                }
            }
            catch (err) {
                console.log(err)
            }
        }
        getToken()
    },[])

    const getProfile = (token) => {
        Axios.get(`${api}/api/profile/get-profile`, {
            headers: {
                'Authorization' : 'Bearer' + token,
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
            }
        })
        .then((res)=> {
            const data = res.data.data.profile
            console.log("data ->",data)
            setData(data)
        })
        .catch((err)=>{
            console.log("Account -> err", err)
        })
    }

    const onSelect = (item) => {
        setSelected(item.total)
        console.log(selected)
    }

    const onTopUpPress = () => {
        const time = new Date().getTime()
        const nominal = moneyField.current.getRawValue()
        const body = {
            transaction_details :{
                order_id : `Donasi-${time}`,
                gross_amount : selected !== null ? selected : nominal,
                donation_id: item.id
            },
            customer_details: {
                first_name: data.name,
                email: data.email
            }
        }
        Axios.post(`${api}/api/donasi/generate-midtrans`, body, {
            timeout: 20000,
            headers: {
                'Authorization' : 'Bearer' + token,
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
            }
        })
        .then((res)=>{
            if(res) {
                const data = res.data.data
                console.log("res -> data",data)
                item.navigation.replace('Payment', data)
            }
            
        })
        .catch((err)=> {
            console.log("err", err)
        })
    }

    const renderItem = ({ item }) => {
        const bgColor = item.total == selected ? colors.blue : colors.white
        const fontColor = item.total == selected ? colors.white : colors.black

        let rupiah = '';
        var nilai = item.total.toString().split('').reverse().join('');
        for(var i = 0; i < nilai.length; i++) if(i%3 == 0) rupiah += nilai.substr(i,3)+'.';

        return (
            <View style={styles.containerBtn}>
                <View style={[styles.btnMenu, { backgroundColor: bgColor }]}>
                    <TouchableOpacity onPress={() => onSelect(item)}>
                        <Text style={{ color: fontColor, fontSize:14, textAlign:'center' }} >Rp. {rupiah.split('',rupiah.length-1).reverse().join('')}</Text>
                    </TouchableOpacity>
                </View>
            </View>

        )
    }

    return (
        <View style={{ flex: 1 }}>
            <View >
                <Text>ISI NOMINAL</Text>
                <TextInputMask
                    type={'money'}
                    options={{
                        precision: 0,
                        separator: ',',
                        delimiter: '.',
                        unit: 'Rp. ',
                        suffixUnit: ''
                    }}
                    value={total}
                    ref={moneyField}
                    placeholder='Rp. 0'
                    underlineColorAndroid={colors.grey}
                    onChangeText={(value) => setTotal(value)}
                />
                <FlatList
                    data={topup_list}
                    renderItem={renderItem}
                    numColumns={topup_list.length / 2}
                />
                <TouchableOpacity style={styles.btnLanjut} onPress={()=> onTopUpPress()}>
                    <Text style={styles.textBtnLanjut}>LANJUT</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Topup;