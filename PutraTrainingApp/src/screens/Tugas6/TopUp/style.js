import {StyleSheet} from 'react-native'
import colors from '../../../style/colors';

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    btnMenu:{
        paddingVertical:18,
        paddingHorizontal:11,
        marginVertical:5,
        alignSelf:'center',
        borderRadius:15,
        elevation:2,
        width: 100,
    },
    containerBtn:{
        marginHorizontal:2,
        width:"32%"
    },
    btnLanjut:{
        padding:18,
        backgroundColor:colors.blue,
        borderRadius:10,
        marginTop:10,
    },
    textBtnLanjut:{
        textAlign:'center',
        color:colors.white,
        fontSize:16
    }
})

export default styles;