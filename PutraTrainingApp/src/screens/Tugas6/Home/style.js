import { StyleSheet, } from 'react-native'

import colors from '../../../style/colors'


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    containerSearch: {
        borderColor: colors.white,
        borderWidth: 1,
        borderRadius: 30,
        paddingHorizontal: 80,
        justifyContent: 'center',
        alignSelf: 'center',
        width:"70%"
    },
    inputText: {
        textAlign:'center',
        fontSize: 16,
        color: colors.white,

    },
    icon: {
        alignSelf: 'center',
        justifyContent: 'center',
        
        color: colors.black
    },
    item: {
        justifyContent: 'center',
        alignSelf: 'center',
        color: colors.white
    },
    text: {
        color: colors.white
    },
    textImage: {
        marginTop: 5,
        color: colors.black,
        textAlign: 'center'
    },
    image: {
        marginHorizontal: 18,
        width:"60%",
    },
    pembatas: {
        borderBottomWidth: 10,
        borderBottomColor: colors.grey
    },
    containerDanaMendesak: {
        marginTop: 10,
        marginHorizontal: 20,
        marginBottom: 5
    },
    textDanaMendesak: {
        fontSize: 20,
        fontWeight: 'bold',
        color: colors.black,
        marginBottom: 10
    },
    containerFlatlist: {
        height: 200,
    },
    listItem: {
        height: 170,
        marginHorizontal: 5,
        borderRadius: 15,
        backgroundColor:colors.white,
        borderColor: colors.white,
        elevation: 5,
    },
    textTotal: {
        fontWeight: 'bold',
        color: colors.black,
        textAlign: 'center'
    },
    headerContainer: {
        backgroundColor: colors.blue,
        height: 150,
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20
    },
    infoContainer: {
        backgroundColor: colors.white,
        paddingHorizontal: 10,
        paddingVertical: 20,
        borderRadius: 10,
        elevation: 10,
        width: "85%",
        height: "15%",
        marginTop: -55,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    infoPosition: {
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    componentSaldo: {
        flexDirection: 'column',
        backgroundColor: colors.blue,
        borderRadius: 5,
        justifyContent: 'center',
        padding: 10
    },
    componentSaldoPosition:{
        flexDirection: 'row', 
        justifyContent: 'space-evenly',
    },
    componentIcon:{
        flexDirection: 'column'
    },
    sliderContainer: {
        marginTop: 15
    },
    menuContainer: {
        flexDirection: 'row',
        justifyContent:'space-between',
        marginTop: 20,
        marginBottom: 15,
        width:"100%"
    },
    listImage:{
        width: 300,
        height: 120, 
        borderTopLeftRadius: 15, 
        borderTopRightRadius: 15
    }
})

export default styles;