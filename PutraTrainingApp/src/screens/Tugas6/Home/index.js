import React, { useState } from 'react'
import { Text, View, Image, TextInput, TouchableOpacity, StatusBar, FlatList, ScrollView, ImageBackground } from 'react-native'
import colors from '../../../style/colors'
import styles from './style'
import Ionicon from 'react-native-vector-icons/Ionicons';
import { SliderBox } from 'react-native-image-slider-box'

const Home = ({navigation}) => {

    const images = [
        require('../../../assets/images/humanity.jpg'),
        require('../../../assets/images/menolong-2.jpg'),
        require('../../../assets/images/menolong-3.jpg'),
    ]

    const list = [
        {
            id: '1',
            desc: 'Bantu sesama',
            total: 'Rp. 2.000.000',
            image: 'https://img.okezone.com/content/2019/10/07/614/2113643/keutamaan-menolong-orang-yang-sedang-susah-mvmStcWXls.jpg'
        },
        {
            id: '2',
            desc: 'Tolong menolong',
            total: 'Rp. 1.000.000',
            image: 'https://storage.nu.or.id/storage/post/16_9/mid/148963880158ca159156198.jpg'
        },
        {
            id: '3',
            desc: 'Bantu sesama',
            total: 'Rp. 1.500.000',
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSsgH34ykyf8_GIsQaWJDz4KCHYVOZbhBJT9A&usqp=CAU'
        },
        {
            id: '4',
            desc: 'Tolong menolong',
            total: 'Rp. 1.000.000',
            image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUTEhIVFRUVFRUVFRUVFRUVFRUVFRUWFhUSFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMsNygtLi0BCgoKDg0OGhAQGC0dHR0tLS0tLS0tLS0rLS0tLS0tLS0tLS0tLSstLS0tLS0tLSstLS0tLSsrLTcrLTItLTg3Lf/AABEIALcBEwMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAEBQMGAAECBwj/xAA8EAABAwIEAwUGBAUDBQAAAAABAAIDBBEFEiExBkFREzJhcYEiUpGhscEHFCNCYnLR4fAkgvEVM1ODov/EABkBAAMBAQEAAAAAAAAAAAAAAAACAwEEBf/EACYRAAICAgIBBAIDAQAAAAAAAAABAhEDIRIxQQQiMlFhcRMUkTP/2gAMAwEAAhEDEQA/APILLSke1chYjTmymjco7LTDqg0aUptY9CFdeKNWQu6t+yo9OdFe+IGf6aA+A+ihm+SKw+LKTimyXxFMa8XBSyMpvAh9CfhtO1tJGByYPotVk3aVQ10avPeFeMmQQiNxsQLJ3g+KF5M3vajyXI4O2XtFm4xqf02xj+Y+m3zXnVFh+aV2ns5XN89P6p9iGKl4JI1GgXMGVrQRyv8AFCVFY9Hn1LSnM6w2+Wq9Q4YrnSRR37zLtP0VNwaE536bu+rrq80MGTMR+8gkctNE6EmCcXxZ4neRXkfakaL1riJxMR8ivInt1I8SrwRB6LpwlESzNY+QTipe8btDelzcqXgoBtORluRY+ilxWklm1LWsaPVxXLlm7o9DBjXFS8sr9NROLi5ujT3nuNvXxUss4fIDobC2Y7kgWXGIVmVmUE2GluZ8kNglI6SQacxp5qWVrgZKPbY24jq2inyaXyk+I0t90moZcrKb1+ZRXHWHiAvIv7bGW872d9AlL5bNp/L7r0/SpLEqPMyNuTsv8Mg7QDqPtf7KjUkt2Vjb/vv83BWSaps6N3kq3RttWVMZ2e1x+BDv6roSEBeGKMuk2/zmrliOLRQtDTplAvp15eaW4G3sY3zu0AuGW5Ab/Eqm4hXOlJcT3inukLVlpjxn8w/K2Pw66WSqvqqZ5yvux21xqL+IUjKttHT3t+tKPZHutI7xVOlkJJJOp1WSnS2CQ/fRNHtMla8DofaHoiC8WVVEhH9k1w+cuFuY+i58ivaKwddjEvCilk0K7EBK12PVTKWIC3O+yZCmyBbkpw11wtOJJ1OyZsRLZtYsstJKHICbrhaY5dWVEIasuXBShacFopNTv0Xo2Nt/0UR6ALzJhXpOLMc6ihLToGi658/cS2PplNqh7JSdp1T90Jcco5qOTAy3fmntJC1YncLq14NxE2OMMPIWSGopsqDfIGoqzej0SgxNkuYDffzU8FezVpXnUFbroSEWMQk95I8f0UWSqsu1FIxjyRte4TyPEQRuvM4K08ym1PiGm6FjpCyyOTLfXVLXMK8wrmgTHpmv81ZJ8Q0OqqtU4l5KdKhGz0nhyqyM0F7gBPJi46OcA23JUHB8Ss0BWOgxATew51jyN9VKeHnvydGP1DxqqtHFfghkcDGLn/NU+4TwgMJzd7QlD8CVbzUTwvdmDQCw87a3Hir0KVuUaC45ry/Ucoy4srPMprR5n+MFhFFprmt6WuqPWGwp/L7q4/jO8NEDed3E/ZULFKgWhtyb917Hob/hiefk+Raa+b2GHolOLVQirI5v2vY0u8nAtcuZq68YQGLHtYY3+4HMPle4+q7CY84ymDGsiZfLYE9NhySbDadoHavF2Ri9ved+1vxTCkd28Eb3G5iYWm/Vug+VkBjDzHAyLm79R/r3R8Fv5ATV9W6V7nuNy43/ALDwUX5d1r2Nk74dwZ013W0HM90eJTqopmMbljBkcP3EWa3y5D1SqLezbKUKVx/afXT6oilpJYntcWHLvy7p3R07mMdmkkzkftZ9C7YfNC11ZJUHMG2a0WFtGtHi480cUFliDQdlG+JJcLrCw5S4Fvnt5JsZ1zyi0WTsilhQzoUX2i5zLAoB7MrEZdaW2ZQiBUjSuCFoFMYSgqYjRD3U8Llphy1uqveBVZkoXxneM/LkqSG6qw8JzWfLH78d/Vv/ACpZlcf0PjeyKGTK8HxTWclxB5JDUmxRcdaQ3VK0NFmsTZmdYJbLRAmyOpZcziSuanQ3WrRr2LXUWVYGJpE3Nqh6gC9lti0C5VNG8hRzaKEvK0wLfUoCee52Um6ljpwd0wpxTyG2ilFW9puCfNd/l7bKF4KAZbOA8dtWR593XZfrfUX+C9za3RfNOBODamJzjlDXtJPkV9KNeCwEG4IFl5nrY+7kUg9UeX/ibRCYubzIbl8HAEj47LyWpik0ux+mndO45L2bjdntuPMBhv8AFVfGHdpDmaSLWzNBO/Vel6b/AIx/RGXyZTTYNGc/7Rv69FkdaSx8eVuV23gW66KGqbZxUbG2IVzBvwjN/wB2N3cyh5/2nX43AW3Qdu90shIjvbTvPPKNg5n6I/hnD4zFO6R2VuZoNtyAL5R8UVhMrB/qXN9lpMdPHyaBo55/iJ5qiWtiBT6WUMaBHYAezC3RjB/Gd3u6pNW0krgTPK2JvIE6/wC2Nq7xTiGd5LWXFzYBu5/qgJqFkQz1b3PkO0DHWP8A7H8vILXL6CiOWoo2ABkZcRu9+Ulx62OyEqqyKSwd2gHQFgA8mgWRtPHK4ZhHHFF0DL6eL3A38yUNURs3dD7PvxkW8zluPkl2aAVFAwDNFJmHNrhZw8bjQpq1tgooYqctNpCDbYhTU5BaPh8FHIvJSDNLFJlXBYpFDV1izKsQZQosuSFIAsIVBCJSMctELSADodUxwKXLVsHvBzfiEnppbFSMqC2ZknuvB+eqWStNGp0xlXaOPmVMSDHdD4hIHOc4bEkhLn1R7t0iVoa6YZSVIaUVJMHBKWBSBxC2gsZ081tELUO9pRia65OpRQWGvaCFDG0bFTxx3UhpuiAoG/La6IhsNkXT0/VGtgCLNSFIhJXLqVOexC4dT+CLChI+nXvXAlT21DCTuG5T5jT7Lxs0ZK9M/DKoPZGP3T/dcnrPgmbFbBOO2fqPaPdZ91T8PkFi0kEO0PgVcOJ35qx7TzY0fVecVTnQyPbyvdd3p9Yor8EZdgGPUpZIQRzSeYWCuONRiaFsje80WcqdWqpgzw2s/TLTq07+B5FW7AooZIGNv3Lh3W5JJPrdUTDLG46hTQYkYnnWzTobfXxWxlRjRYeJMXjhf2dMG57e1IOQ6NPXxWcOcGSz/rVB7Nh1F++/x17o807wDhONuWoe10kjmhwa+wjjLhewFiXHXyTmsxB7O8y4/gdmt/tIHyU5ZoXVlFhnV0Dy4QyNmSNwLbbHS/rzXnuK0PZOJB7N5PI+yfAq4VGJ9qCYm9pbcFzhr0VaxV8jwRNEGttoNd9+V9VXkT4laqY8wvYNcN7bO9Oq3hVUQ4NOzj8+ql7Zou0Ny35bjzF9kGyM9o23vD6pJI1MsZjUZainKJwUSpDlWLotK0gBKVsLcgXATiM7AWOathdAIAhGiny5guXNW4zZADHFI8mngD8RdJwm1c/tGA8wAPholmWyRdDEjHIhguhmhFQsKw022JERReCmgpnHYJnBhr+ixs2gaKAo2mgTCnw7qmMFEAlsahW2nPRTRUJcQOZIA9U4FMpsXkZBJCxrLnOwOcb6kuAJCSU6KY8Tn0L8VoIqctaZLuNhtYXJtZc/lwuePHtDgcpBzDW3jdOMQoBGwHPd1ruFtBzsOayM9bK5cO/ahK+JWT8P3ZZnAmwIGnVIrJjgEuSdp5HRZnV42cy7OeLJcuJOBNgWC3mLf1VT4iiDpCRvuU0/FFx7YSAnNpZUv/qMj3EEjzXZifsj+iD7YbQ1RYD05jwSrHKYZg5mx18l32rnOy7nbQak8k2j4ekIGd8bT7pcS4dbhoPwTOSGhjlP4qyvYXCc4PRXXh3gcPk7epFogbsiO7+d3Dk3w5p3gGCUsDc7XCaQbuIFmn+FnL1uUVX4lbmuLLnb9sTsxel47n/gdX14AsNNNlUsXxLfVB4tjVr6qs1NW55UseNstkypEWKTBzsw0IO4XcGOyCweQ9o5OAJ8r7qB8eiX7u81247Wjgy09h9bJHIS6NnZ9Bmc74k7KfB6O5znYd3xPVAYdBnka1xsL2VscwAWtoNk8peCcUDuUZROULgxpByWGjBANzr5LSiylYsAr0qiup5QoCE6FO2lSNUIUjStMJrLgtXca7cEAEYdCXnIOaKZw7IXHTZG8ExZqlvgCfgvQ4qYNaTZQy5OLorCFo8+p+GiO8mlLgsbdTZH4xibWXA3VZmxB7jvYJE2xqSLMwRN6KdlQzlZUzM7qURC5w5lPxCy5RtBUkbEjoat2xTmCclK0AZE32m9Mw+qF4lu6eLb/ux7cvbC3imKObPBA3RvaMvb+YXv80ZJQOlmDhZoZI15LtNA4Gw8VzylZ6OLFwW32IuOf3+d1BjtY90sdi6zow6w1B0sdPNWfG6Ok1Mry4+6w2B8Cf6IDCMWDpRFTwBoa2xcbuc1g2u46+iVSGavYPh0bjEzM1zXAWIcCDobDQ+FlO2Mgg9Exxaoii1cSX2vYaAeJPJIKbiBr7kNdYHcHT5qiyP6OV4FfYF+INT2kLJPEt+CR8HcNvqc7iezjba73NJvfkxo1cd1cBjTNiy9utj9l1U8R2Git/YpUkS/qfcgfC+HKWmlEkjnyOv7GawYDbnl3OvP+6sNQGPYWljS3pYW9FVo6p9QTc20I11uDyPghWY46AGJ2YgA5XO1N9btLsoBtvfx8FOS/k9y7+hsc3hfB9PyQ4vRfl3GSCUjmY3G/wACfoUiqMedINLg7EeKgxTEHyXc46IHDYy+5A6D+6pGFr3BPJvRIQTut5ANToPFM4aHmUixRxzEX2VlBnPLIvAfH2DmBzpbDW7djpySmpmZf2BoOo+aEIXQCdJIi22EUktnNPRwKu0sOaxVEjKvtBPmia7wCJjQBHwELTbo17wonAKYxEsWWWIArMwUBRMyGKYU0umlcraYwJiKmIQjHIuF10AHYRiDqd/aN3sQR1B5K7niVkkALe9bUc7rz94WqaUtKnPGpFIya0OJrvJcVkcKLpog4XRLYrKQ4NHSollOiIoroxkYCYwhjgsEzwvVzfMfVCPKPwVl5WDq4LJdGx7QFUxxuroiwOuJQSCbjS5P0TDiTFr2hhaXOv3WD2nu8APqn8uFRj2sga6xDS3WTXvW8/FRUVL2Isxgizd4kZ5XeZ5eV/RcR6zkvBW8J4Xlec9U/KNzHHq7+UvO5/lHqmkmdjckDY6aP3nd4/xWGpPibJvM9rW2F233LvacfnYDwCRT0sRcS5znD3S/KP8A5H3WiX9iSqjh1zzOkJPtF2jfRoP1KU4riga3s6ezR/ANT4lxTzFqyaMXp4mtA/8AGGn46ZiqZieNzS3D3XHMWCeKbJTaRJJNNYe2Bm62XDInD2pJB5XStoN1IInHqrKJzuQ4pcaEdy0XPJJqyV0r8ztXHXl18Rop4oh1v5f1R0mX9osLAWsNDzObcqsMdOyGXLaoCqqDMLZx5AH5pnhlII25R/yUMQj6U3CvGKRGU3IkmFgqTij7vKu9Y0ZCqFVn2j5p5CEC7auQumhIaZzV14Xfmgt0JCpRCtnBUnsyN6EFZLoaPY1lgsoZBZMn2QUzLFSKEAcsUlliAKrO1COTGsZql706EZwurrVlsBMYbCkjcowthABzJVu4QbCpcyDS18PHM23RPWUarnBhzOIV27Ky556ZWPQI2Cy6axFiNSR03MrLNARTaqzYNhoi9s98jQW7oP3Sh0rG7HMeg+6LGLSDz311HkiUJSQ0Jxi7Y4ZCGEuzEF2+pv8AHkga3FcndZfxGqCkr3PNjp18fJcS1QA0Cg48ezsUlNWmJcd4lcAcwI8FTKnH5HnQ2CYcY4wJP0wBodT9lVomk6BVjHVshOVOkMI8WkB7x+K4xKsMxzOAv1A19TzTGh4Pq5bER5Qf3P8AZHz1PordhP4fxMF53GR3Rt2sH3P+aJ9Im26PNYSb6KOcm4BJ8l6zjGGMgpniNjGi24aAb+e68km7x81aMfJzzn4D6Z2iOZqllO5MonaKhM6ujqQpcHaplQt1WoDnFJLRlUaY6lXnGgBGSVRZDqhgchSWXDQiGs+iwCJ6ecHy2kc2+409EjeFPhc2SVruhCGtGrsvrioXot8Vxf1UQjvooFQUrFOaZYgBBWRpRK1PqkJRM1MhWBLYW3BaCcU2F0uVsBAHQW3OXK2wXQBYuD6xkUhdIbBXduP0x2kB8BuvLXOsLBdUcgBvbVI4J7HUqPSpeIoxtG4/BcScQhwsWFo8b/VUaaodbM06jl1UlNiziOo5tKFBIHIutFLG4WaXA+DgT/ddStP7ZD67+qqMNU0G4JHl9wm5r8w1PtAaEb+iYUN/6s6M5ZB68vQrmtxf9NxjObTboq1iVe57S07j/A4JY2odYcljipLY0ZuLtDPAsIkr6gtboO9I+2jW/wBTyXruGYFBTMDYomi37iAXk+8XHW6T/hvHGKNrmNs5zndoeZc0kX8rKzPcuaT3RXvZDIVCXKSRy5ahGA2NURkpHnk0rwzEG2kcPFfRUTBJS1EVxe2Yf56L56x5lpnDxXRhdxa+mQn2R07kyjdolEB1TXl6KyEOBJqm+HS2IKrr3apzhcosFoEnEUl2qmOKtXEs2iqpWS7NJIG3UsgIWUrV3XixWADPOuq5YtuC0wEmw3Oy0D0nDnExMubnKFsnVbooC2NgO4aLrt4UGVREXrForaw0SVLdErmam9QEsmC1GMAlaoCi5Ah3hMKcgrq64WymMOl0X2F1y1D1L9bIAnp3XKJa1DUgRjkAZJJYBcFpaQ9ux3UdQVNTOuLIAlediOaIbMbaHZBxm1x0U0R0JQBt8mYqEhdN5eafcMcPuqpRcfptN5Hcrb5B4lY3RvZfuB6QxUUebQuzPI/mNx8rJ2+RcEaWGw0HoonNK5Lt2WJHTLV7ocRlSMWgIsWq3NqGNa4jR17G2lua8tx8fquI2udVe8Qqx+cF9u78VVuLYQJDYabrtjGonM3sQ0veCdPboksG6dw6tTIwVzhGYRPrZCVbVFTPLSgA3iN2oSNrU7rSJB4hLtG621WMDuiFjrso8QN3BExMc4tDWkuOwG6uGCcMZHCWezn7hvJvn1KyTSRqViLD+E5ZWhzj2YOwI1t1snuGcLxwHN33DmeXkFY3hQOaouTZTiiArh7Vp5UT36LBjrKsQ/a+fwWIAW1TErnandaxKKhiEDFz1A8Ip6isnFB8q1ZEOaonNQYc8kFuUbIfZQsbblMYH0rdF286rTeQXDnIAjlNyp4DZQsZzXcTrlAEx0ddT8h4lQWvZTgXdYa8h5lADjhLB/zU4Yb5GgveR05D1Xr1NSMiYGRtDWjYD6+ar3BmCmlhzPH6kliRzA5N/wA6p26YrmnJtloqkSFoCisopHlcAlLRpOseNPRDm62Atow8zxN7vzJtuHhccUU5BuRuEbxgww1IkA6H1C4xavbUxBw3G4XbF6OdrZUoGappRu0QTm2FhzKIo76pkYQ17beaDLLWRVQ9vNwQb6pg2BcfgFgBYtYJjhPDclQc17M94/YKtSVbnaDToAvYMCpRFTxs5hov5ndJknS0NGNsiwrB4qcew27ubjqSipnqeeTTQXULhpcqF2V6IQ9DzAqSN3hZdlAAj2XCFe1GyBQOcEADjyWlPp0WIsAR+oSmqjW1i1AxRM1QNWLE4pM1l1HPHZYsQAHUbLVM3VYsTChLSo2i6xYgDJnWFlkI0v1WLEAGxNs269F/D/hoNaKiUAk6sG4H8R8VixSytqI8FsuczyoXNWLFAoQuaVp7DZYsWtga7InmsMbuq0sRYUKeI8HE0Rv3hqCvNKW7ZQ3xIKxYunC9Esi2RYm4hxtolMjzfc/FYsVWTB3rgrSxYAz4aou2qY2cs1z5N1XrzmfALFilk7KQNuFlG5yxYpjkEjdFFyWLEGHHZ3Wn0yxYtMI+wWLFiAP/2Q=='
        }
    ]

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
            <ScrollView>
                <View style={styles.headerContainer}>
                    <View style={styles.header}>
                        <Image source={require('../../../assets/images/heart.png')} style={{ width: 35, height: 35, marginLeft: 10, marginTop:5 }} />
                        <View style={styles.containerSearch}>
                            <TextInput
                                style={styles.inputText}
                                placeholder="Cari disini"
                                placeholderTextColor={colors.white}
                            ></TextInput>
                        </View>
                        <TouchableOpacity>
                            <Ionicon style={styles.icon} name="heart-outline" size={35} style={{ marginRight: 12, marginTop:5, color: colors.white }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.infoContainer}>
                    <View style={styles.infoPosition}>
                        <View style={styles.componentSaldo}>
                            <View style={styles.componentSaldoPosition}>
                                <Image source={require('../../../assets/images/wallet.png')} style={{ width: 25, height: 25, marginRight: 10 }} />
                                <Text style={styles.text}>Saldo</Text>
                            </View>
                            <Text style={styles.text}>Rp. 0</Text>
                        </View>
                        <TouchableOpacity style={styles.componentIcon}>
                            <Ionicon style={styles.icon} name="add-circle-outline" size={35} />
                            <Text style={{ textAlign: 'center' }}>Isi</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.componentIcon}>
                            <Ionicon style={styles.icon} name="repeat-outline" size={35} />
                            <Text>Riwayat</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.componentIcon}>
                            <Ionicon style={styles.icon} name="grid-outline" size={35} />
                            <Text>Lainnya</Text>
                        </TouchableOpacity>

                    </View>

                </View>
                <View style={styles.pembatas}>
                    <View style={styles.sliderContainer}>
                        <SliderBox
                            autoplay
                            circleloop
                            images={images}
                            sliderBoxHeight={200}
                            dotColor={colors.blue}
                            inactiveDotColor={colors.grey}
                            imageLoadingColor={colors.blue}
                            ImageComponentStyle={{ width: "92%", borderRadius: 10 }}
                        />
                    </View>

                    <View style={styles.menuContainer}>
                        <TouchableOpacity onPress={() => navigation.navigate('DaftarDonasi')}>
                            <View style={styles.image}>
                                <Image source={require('../../../assets/images/donation.png')} style={{ width: 50, height: 50 }} />
                                <Text style={styles.textImage}>Donasi</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate('Statistics')}>
                            <View style={styles.image}>
                                <Image source={require('../../../assets/images/bargraph.png')} style={{ width: 50, height: 50 }} />
                                <Text style={styles.textImage}>Statistik</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate('Riwayat Transaksi')}>
                            <View style={styles.image}>
                                <Image source={require('../../../assets/images/history.png')} style={{ width: 50, height: 50 }} />
                                <Text style={styles.textImage}>Riwayat</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate('Buat Donasi')}>
                            <View style={styles.image}>
                                <Image source={require('../../../assets/images/unite.png')} style={{ width: 50, height: 50 }} />
                                <Text style={styles.textImage}>Bantu</Text>
                            </View>
                        </TouchableOpacity>


                    </View>
                </View>
                <View style={styles.containerDanaMendesak}>
                    <Text style={styles.textDanaMendesak}>Penggalangan Dana Mendesak</Text>
                    <View  >
                        <FlatList
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            style={styles.containerFlatlist}
                            data={list}
                            keyExtractor={(item) => item.id}
                            renderItem={({ item }) =>
                                <View style={styles.listItem}>
                                    <Image source={{ uri: item.image }} style={styles.listImage} />
                                    <View >
                                        <Text style={styles.textImage}>{item.desc}</Text>
                                        <Text style={styles.textTotal}>{item.total}</Text>
                                    </View>
                                </View>
                            }
                        />
                    </View>

                </View>

            </ScrollView>
        </View>
    )
}

export default Home;