import React, { useEffect } from 'react'
import { Text, View, TouchableOpacity } from 'react-native';
import styles from './style'
import Icon from 'react-native-vector-icons/MaterialIcons';
import MapboxGL from '@react-native-mapbox-gl/maps';

MapboxGL.setAccessToken('pk.eyJ1IjoicmFoaW1zeWFocHV0cmEiLCJhIjoiY2tqYWYzMmh5MGEwdTJycXYwb3pwamh2biJ9.rK_ZiIGvygHYo_Wb_qEeTw')

const coordinates = [
    [107.580110, -6.890066],
    [106.819449, -6.218465],
    [110.365231, -7.795766]
]

const Maps = () => {

    useEffect(() => {
        const getLocation = async () => {
            try {
                const permission = await MapboxGL.requestAndroidLocationPermissions()
            } catch (error) {
                (console.log(error))
            }
        }
        getLocation()
    }, [])

    return (
        <View style={styles.container}>
            <MapboxGL.MapView
                style={{ flex: 1 }}
                zoomEnabled={true}
            >
                <MapboxGL.UserLocation
                    visible={true}
                />
                <MapboxGL.Camera
                    followUserLocation={true}
                />
                {
                    coordinates.map((item, index) => {
                        return (
                            <MapboxGL.PointAnnotation
                                key={index}
                                id={`pointAnnotation${index}`}
                                coordinate={item}
                            >
                                <MapboxGL.Callout
                                    title={`Longitude ${item[0]} \n Latitude ${item[1]}`}
                                />
                            </MapboxGL.PointAnnotation>
                        )
                    })
                }

            </MapboxGL.MapView>

            <View style={styles.detailContainer}>
                <View style={styles.pembatas}>
                    <TouchableOpacity >
                        <View style={styles.posisi} >
                            <Icon style={styles.item} name="home" size={25} />
                            <Text style={styles.text}>Jakarta, Bandung, Yogyakarta</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.pembatas}>
                    <TouchableOpacity >
                        <View style={styles.posisi} >
                            <Icon style={styles.item} name="email" size={25} />
                            <Text style={styles.text}>customer_service@yukdonasi.com</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.pembatas}>
                    <TouchableOpacity >
                        <View style={styles.posisi} >
                            <Icon style={styles.item} name="call" size={25} />
                            <Text style={styles.text}>(021) 777 - 888</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default Maps;