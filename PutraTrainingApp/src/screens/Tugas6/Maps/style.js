import { StyleSheet, } from 'react-native'

import colors from '../../../style/colors'

const styles = StyleSheet.create({
    container: {
        flex: 2,
        
    },
    posisi: {
        flexDirection: 'row',
        backgroundColor: colors.white,
    },
    detailContainer:{
        flex:1,
        borderTopWidth:10,
        borderColor:colors.lightGrey,
        backgroundColor:colors.white
    },
    text: {
        fontSize: 15,
        alignSelf: 'center',
    },
    item: {
        marginRight: 10,
        padding:15
    },
    pembatas: {
        borderBottomColor: '#bdc3c7',
        borderBottomWidth: 1,
    },
})

export default styles;