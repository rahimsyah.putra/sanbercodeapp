import { StyleSheet } from "react-native";
import colors from "../../../style/colors";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#bdc3c7',
    },
    tabBar: {
        backgroundColor: colors.blue,
        padding: 20,
        justifyContent:'center'
    },
    textBar: {
        fontWeight: 'bold',
        color: colors.white,
        fontSize: 20,
    },
    containerHeader:{
        flexDirection:'row',
        justifyContent:'space-between',
        backgroundColor: colors.blue
    },
    textLogo: {
        fontSize: 20,
        fontWeight: 'bold',
        color: colors.white,
        marginEnd:50
    },
    textLogoContainer:{
        flex:1,
        alignSelf:'center',   
    },
    item:{
        alignSelf:'center',
        marginLeft:10,
        marginTop:5,
        color:colors.white
    },
    accountBar: {
        flexDirection: 'column',
        backgroundColor: colors.white,
        borderBottomColor: '#bdc3c7',
        borderBottomWidth: 5,
        padding:5
    },
    outBar: {
        backgroundColor: colors.white,
        borderBottomColor: '#bdc3c7',
        borderBottomWidth: 5,
        padding: 5,
    },
    pembatas: {
        borderBottomColor: '#bdc3c7',
        borderBottomWidth: 1,
        marginTop:10
    },
    settingBar: {
        flexDirection: 'column',
        backgroundColor: colors.white,
        padding: 5,
        borderBottomColor: '#a6a6a6',
        borderBottomWidth: 5,
    },
    posisi: {
        flexDirection: 'row',
        backgroundColor: colors.white,
    },
    posisiBtnOut: {
        flexDirection: 'row',
        backgroundColor: colors.white,

    },
    textNama: {
        fontWeight: 'bold',
        marginLeft: 5,
        alignSelf: 'center',
        color:colors.black,
    },
    photo: {
        height: 50,
        width: 50,
        borderRadius: 20,
        marginHorizontal:10,
        marginBottom:10
    },
    text: {
        fontSize: 15,
        alignSelf: 'center',
    },
    textSaldo: {
        marginLeft: 150,
        alignSelf: 'center',
    },
    item: {
        marginRight: 10,
        padding:15
    },
    profileContainer:{
        alignSelf:'center',
        justifyContent:'center',
        backgroundColor:colors.white,
        height:120,
        width:120,
        marginTop:20
    },
    itemInPhoto:{
        position:'absolute',
        width:20,
        height:20,
        backgroundColor:colors.grey,
        left:50,
        top:45,
        borderRadius:20,
        justifyContent:'center',
        alignItems:'center'
    },
    icon:{
      alignSelf:'center',
      justifyContent:'center'  
    },
    editItem:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginVertical:5,
        marginHorizontal:15
    },
    editTitle:{
        marginLeft:20,
        fontSize:14,
        color:colors.grey
    },
    editPhoto: {
        height: 100,
        width: 100,
        marginHorizontal:10,
        borderRadius:30
    },
    camFlipContainer:{
        marginLeft:20,
        marginTop:20,
    },
    btnFlip:{
        backgroundColor:colors.grey,
        borderRadius:40,
        width:50,
        height:50,
        justifyContent:'center',
        alignItems:'center'
    },

    btnTakeContainer:{
        marginTop:450,
        alignSelf:'center'
    },
    btnTake:{
        backgroundColor:colors.grey,
        borderRadius:45,
        width:90,
        height:90,
        justifyContent:'center',
        alignItems:'center'
    },
    btnSave:{
        backgroundColor:colors.blue,
        borderRadius:50,
        padding:10,
        alignSelf:'center'
    },
    textBtnSave:{
        color:colors.white,
        fontSize:16,
        fontWeight:'bold'
    },
})

export default styles;