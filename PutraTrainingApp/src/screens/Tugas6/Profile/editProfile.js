import React, { useEffect, useRef, useState } from 'react'

import styles from './style';
import colors from '../../../style/colors'
import Ionicon from 'react-native-vector-icons/Ionicons';
import Axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import api from '../../../api';
import { RNCamera } from 'react-native-camera';
import { View, Text, Image, TouchableOpacity, Modal, TextInput } from 'react-native';

const editProfile = ({ navigation, route }) => {
    const { item } = route.params;
    console.log("data berhasil dipindahkan", item)
    let input = useRef(null)
    let camera = useRef(null)
    const [isVisible, setIsVisible] = useState(false)
    const [editable, setEditable] = useState(false)
    const [token, setToken] = useState('')
    const [name, setName] = useState(item.name)
    const [type, setType] = useState('back')
    const [photo, setPhoto] = useState(null)

    const toogleCamera = () => {
        setType(type == 'back' ? 'front' : 'back')
    }

    const takePicture = async () => {
        const options = {quality: 0.5, base64: true}
        if (camera) {
            const data = await camera.current.takePictureAsync(options)
            //console.log("editProfile -> dataPhoto ", data)
            setPhoto(data)
            setIsVisible(false)
        }
    }

    useEffect(() => {
        const getToken = async () => {
            try {
                const token = await AsyncStorage.getItem("token")
                if (token != null) {
                    return setToken(token)
                }
                //console.log("getToken -> token", getProfile())

            } catch (err) {
                console.log(err)
            }
        }
        getToken()
    }, [])

    const editData = () => {
        setEditable(!editable)
    }

    const onSavePress = () =>{
        const formData = new FormData()
        formData.append('name', name),
        formData.append('photo', {
            uri: photo.uri,
            name: 'photo.jpg',
            type: 'image/jpg'
        })
        Axios.post(`${api}/api/profile/update-profile`, formData, {
            timeout:20000,
            headers : {
                'Authorization' : 'Bearer' + token,
                Accept: 'application/json',
                'Content-Type' : 'multipart/form-data'
            }    
        })
        .then((res)=> {
            //console.log("editProfile -> res", res)
            alert("Update Profile Success")
            navigation.navigate('Profile')
        })
        .catch((err)=> {
            console.log("editProfile -> err", err)
            alert("Update Profile Failed")
        })
        
    }

    const renderCamera = () => {
        return (
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{ flex: 1 }}>
                    <RNCamera
                        style={{ flex: 1 }}
                        type={type}
                        ref={camera}
                    >
                        <View style={styles.camFlipContainer}>
                            <TouchableOpacity style={styles.btnFlip} onPress={()=> toogleCamera()}>
                                <Ionicon name="camera-reverse-outline" size={35} />
                            </TouchableOpacity>
                        </View>
                      
                        <View style={styles.btnTakeContainer}>
                            <TouchableOpacity style={styles.btnTake} onPress={()=> takePicture()}>
                                <Ionicon name="camera-outline" size={50} />
                            </TouchableOpacity>
                        </View>
                    </RNCamera>
                </View>
            </Modal>
        )
    }

    return (
        <View style={styles.container, { backgroundColor: colors.white }}>
            <View style={styles.containerHeader}>
                <TouchableOpacity
                    onPress={() => navigation.navigate('Profile')}>
                    <Ionicon style={styles.item} name="chevron-back" size={25} color={colors.white}/>
                </TouchableOpacity>
                <View style={styles.textLogoContainer}>
                    <Text style={styles.textLogo}>Profile Saya</Text>
                </View>
            </View>
            <View style={styles.profileContainer}>
                <TouchableOpacity 
                    onPress={()=> setIsVisible(true)}>
                    <Image source={item.photo !== null && photo == null || item.photo == null ? { uri: `${api}/${item.photo}`+ '?' + new Date().getTime(), cache: 'reload', headers: {Pragma: 'no-cache' }} : { uri: photo.uri }} style={styles.editPhoto} />
                    <View style={styles.itemInPhoto}>
                        <Ionicon name="camera-outline" size={15} color={colors.white} style={styles.icon} />
                    </View>

                </TouchableOpacity>

            </View>

            <View style={styles.detailUser}>
                <View style={styles.editContainer}>
                    <View>
                        <Text style={styles.editTitle}>Nama Lengkap</Text>
                        <View style={styles.editItem}>
                            <TextInput
                                ref={input}
                                value={name}
                                editable={editable}
                                style={styles.textNama}
                                onChangeText={(value) => setName(value)}
                            />
                            <Ionicon name="pencil-sharp" size={20} color={colors.grey} style={styles.icon} onPress={() => editData()} />
                        </View>
                    </View>
                    <View>
                        <Text style={styles.editTitle}>Email</Text>
                        <View style={styles.editItem}>
                            <Text
                                style={styles.textNama}>
                                {item.email}
                            </Text>
                        </View>
                    </View>
                </View>
                <View style={{ marginTop: 30 }}>
                    <TouchableOpacity style={styles.btnSave} activeOpacity={0.7} onPress={()=> onSavePress()}>
                        <Text style={styles.textBtnSave}>SIMPAN</Text>
                    </TouchableOpacity>
                </View>
            </View>
            {renderCamera()}
        </View>
    )
}

export default editProfile;