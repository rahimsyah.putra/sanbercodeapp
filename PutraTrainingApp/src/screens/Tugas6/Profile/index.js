import React, { useEffect, useState } from 'react'
import { View, Text, Image, StatusBar, TouchableOpacity } from 'react-native'
import styles from './style'
import Ionicon from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import api from '../../../api'
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';

const Profile = ({ navigation }) => {

    const [data, setData] = useState({})
    const [userInfo, setUserInfo] = useState(null)
    const [isGoogle, setIsGoogle] = useState(false)

    const [image, setImage] = useState('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAACWCAMAAACsAjcrAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAADUExURb3Dx48rgwcAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAA0SURBVHhe7cEBDQAAAMKg909tDwcEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHCsBnXGAAHkmh21AAAAAElFTkSuQmCC')

    useEffect(() => {
        const getToken = async () => {
            try {
                const token = await AsyncStorage.getItem("token")
                if (token != null) {
                    return getProfile(token)
                }
                //console.log("getToken -> token", getProfile())

            } catch (err) {
                console.log(err)
            }
        }
        getToken()
        getProfile()
        getCurrentUser()
    }, [userInfo])

    const getCurrentUser = async () => {
        try {
            const userInfo = await GoogleSignin.signInSilently()
            if (userInfo) {
                //console.log("getCurrentUser -> userInfo", userInfo)
                setUserInfo(userInfo)
                setIsGoogle(true)
            }
        } catch (error) {
            setIsGoogle(false)
        }
    }

    const getProfile = (token) => {
        Axios.get(`${api}/api/profile/get-profile`, {
            timeout: 20000,
            headers: {
                "Authorization": "Bearer" + token,
            }
        })
            .then((res) => {
                const data = res.data.data.profile
                setData(data)
                //console.log("data didapatkan -> ", res.data.data.profile)
            })
            .catch((err) => {
                console.log("Profile -> err", err)
            })
    }

    const onLogoutPress = async () => {
        try {
            if (userInfo !== null) {
                await GoogleSignin.revokeAccess()
                await GoogleSignin.signOut()
            }
            await AsyncStorage.removeItem("token")
            navigation.reset({
                index: 0,
                routes: [{ name: 'Login'}]
            })
        } catch (err) {
            console.log(err)
        }
    }

    const onPicturePress = (data) => {
        setImage(data.photo)
        
        navigation.navigate('editProfile', {
            item: data
        })
    }

    return (
        < >
            <StatusBar barStyle="light-content" backgroundColor='#3498db' />
            <View style={styles.container}>
                <TabBar />
                <View style={styles.accountBar}>
                    <View style={styles.pembatas}>
                        <View>
                            <TouchableOpacity style={styles.posisi}
                                onPress={() => onPicturePress(data)}>
                                <Image source={isGoogle == false ? data.photo == null ? { uri : image } : { uri: `${api}/${data.photo}` + '?' + new Date().getTime(), cache: 'reload', headers: { Pragma: 'no-cache' } } : { uri: userInfo && userInfo.user && userInfo.user.photo }} style={styles.photo} />
                                <Text style={styles.textNama}>
                                    {isGoogle == false ? data.name : userInfo && userInfo.user && userInfo.user.name}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.posisi}>
                        <Ionicon style={styles.item} name="wallet-outline" size={25} />
                        <Text style={styles.text}>
                            Saldo
                    </Text>
                        <Text style={styles.textSaldo}>
                            Rp. 120.000.000
                    </Text>
                    </View>
                </View>
                <View View style={styles.settingBar}>
                    <View style={styles.pembatas}>
                        <View style={styles.posisi}>
                            <Icon style={styles.item} name="settings" size={25} />
                            <Text style={styles.text}>
                                Pengaturan
                        </Text>
                        </View>
                    </View>
                    <View style={styles.pembatas}>
                        <TouchableOpacity onPress={()=> navigation.navigate('Bantuan')}>
                            <View style={styles.posisi} >
                                <Icon style={styles.item} name="help-outline" size={25} />
                                <Text style={styles.text}>Bantuan</Text>
                            </View>
                        </TouchableOpacity>


                    </View>
                    <View style={styles.posisi}>
                        <Ionicon style={styles.item} name="receipt-outline" size={25} />
                        <Text style={styles.text}>
                            Syarat & Ketentuan
                        </Text>
                    </View>
                </View>
                <View View style={styles.outBar}>
                    <View style={styles.posisiBtnOut}>
                        <Ionicon style={styles.item} name="log-out-outline" size={25} />
                        <Text style={styles.text}
                            onPress={() => onLogoutPress()}>
                            Keluar
                        </Text>

                    </View>
                </View>
            </View>
        </>
    )
}

const TabBar = () => {
    return (
        <View style={styles.tabBar}>
            <Text style={styles.textBar}>
                Account
                </Text>
        </View>
    )
}


export default Profile;