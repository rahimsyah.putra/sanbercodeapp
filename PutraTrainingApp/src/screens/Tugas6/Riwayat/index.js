import React, { useEffect, useState } from 'react'
import { Text, View, StatusBar, FlatList, ActivityIndicator  } from 'react-native';
import styles from './style'
import colors from '../../../style/colors'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import api from '../../../api'

const Riwayat = () => {

    const [data, setData] = useState([])
    const [isLoading, setIsLoading] = useState(false)

    console.log("getHistory -> data", data)

    useEffect(() => {
        const getToken = async () => {
            try {
                const value = await AsyncStorage.getItem('token')
                if (value !== null) {
                    console.log("getToken ->", value)
                    return getHistory(value)
                }
            } catch (e) {
                console.log(e)
            }
        }
        getToken()
        getHistory()
    }, [])

    const getHistory = (token) => {
        setIsLoading(true)
        Axios.get(`${api}/api/donasi/riwayat-transaksi`, {
            timeout: 20000,
            headers: {
                "Authorization": "Bearer" + token,
            }
        })
            .then((res) => {
                setIsLoading(false)
                const dataSmt = res
                //console.log("getHistory -> data", dataSmt)
                setData(dataSmt.data.data.riwayat_transaksi)
                // console.log("getHistory -> data", data)

            })
            .catch((err) => {
                console.log("getHistory -> err", err)
            })
    }

    if(isLoading){
        return <ActivityIndicator size='large' color={colors.blue}/>
    }

    const RenderItem = ({ item }) => {
        
        let orderId = item.order_id;
        let date = item.created_at;

        let rupiah = '';
        var nilai = item.amount.toString().split('').reverse().join('');
        for(var i = 0; i < nilai.length; i++) if(i%3 == 0) rupiah += nilai.substr(i,3)+'.';

        return(
            <View style={{padding:20}}>
                <Text style={styles.textNominal}>Total Rp. {rupiah.split('',rupiah.length-1).reverse().join('')}</Text>
                <Text>Donation ID: {orderId}</Text>
                <Text>Tanggal Transaksi: {date}</Text>
            </View>
        )
    }

    const renderSeparator = () => {
        return(
            <View 
             style={{
                height:5,
                width:"100%",
                backgroundColor:colors.lightGrey,
             }}
            />
        )
    }

    return(
        <View style={styles.container}>
            <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
            <FlatList
                data={data}
                // style={{maxWidth:200}}
                keyExtractor={(item) => item.id}
                renderItem={RenderItem}
                ItemSeparatorComponent={renderSeparator}
            />
        </View>
        
    )
}

export default Riwayat