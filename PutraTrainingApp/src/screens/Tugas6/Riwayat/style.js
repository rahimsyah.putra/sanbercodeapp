import { StyleSheet, } from 'react-native'

import colors from '../../../style/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:colors.white
    },
    textNominal:{
        fontWeight:'bold',
        fontSize:16
    },
})

export default styles;