import React from 'react'
import { View, Text, Image, StatusBar, SafeAreaView } from 'react-native'
import styles from '../../../style/style'
import colors from '../../../style/colors'
import Button from '../../../components/Button'
//import module  react native app intro slider
import AppIntroSlider from 'react-native-app-intro-slider'

// data yang akan kita tampilkan sebagai onboarding aplikasi
const data = [
    {
        id: 1,
        image: require('../../../assets/images/sublogo-1.png'),
        description: 'Hidup ini indah' + "\n" + 'seindah saat kita membantu sesama',

    },
    {
        id: 2,
        image: require('../../../assets/images/sublogo-2.png'),
        description: 'Membantu sesama' + "\n" + 'tidak akan membuatmu menjadi miskin'
    },
    {
        id: 3,
        image: require('../../../assets/images/sublogo-3.png'),
        description: 'Sekecil apapun pertolongan' + "\n" + 'pasti bermanfaat untuk orang lain'
    }
]

const Intro = ({ navigation }) => {

    //tampilan onboarding yang ditampilkan dalam renderItem
    const renderItem = ({ item }) => {
        return (
            <View style={styles.listContainer}>
                <View style={styles.listContent}>
                    <Image source={item.image} style={styles.imgList} resizeMethod="auto" resizeMode="cover" />
                </View>
                <Text style={styles.textList}>{item.description}</Text>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>
                <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
                <View style={styles.textLogoContainer}>
                    <Text style={styles.textLogo}>Yuk Donasi</Text>
                </View>
                <View style={styles.slider}>
                    {/* contoh menggunakan component react native app intro slider */}
                    <AppIntroSlider
                        data={data} //masukan data yang akan ditampilkan menjadi onBoarding, dia bernilai array
                        renderItem={renderItem} // untuk menampilkan onBoarding dar data array
                        renderNextButton={() => null}
                        renderDoneButton={() => null}
                        activeDotStyle={styles.activeDotStyle}
                        keyExtractor={(item) => item.id.toString()}
                    />
                </View>
                <View style={styles.btnContainer}>
                    <Button style={styles.btnLogin} onPress={() => navigation.navigate('Login')}>
                        <Text style={styles.btnTextLogin}>MASUK</Text>
                    </Button>
                    <Button style={styles.btnRegister} onPress={() => navigation.navigate('Register')}>
                        <Text style={styles.btnTextRegister}>DAFTAR</Text>
                    </Button>
                </View>
            </View>
        </SafeAreaView>
    )
}

export default Intro