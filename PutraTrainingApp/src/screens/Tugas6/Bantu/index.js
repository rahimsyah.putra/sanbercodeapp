import React, { useEffect, useRef, useState } from 'react'
import { Text, View, TouchableOpacity, StatusBar, TextInput, Modal, Image } from 'react-native';
import styles from './style'
import colors from '../../../style/colors'
import Ionicon from 'react-native-vector-icons/Ionicons';
import { TextInputMask } from 'react-native-masked-text'
import { RNCamera } from 'react-native-camera';
import Axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import api from '../../../api';

const Bantu = ({navigation}) => {

    let moneyField = useRef(null)
    let camera = useRef(null)
    const [token, setToken] = useState('')
    const [total, setTotal] = useState('')
    const [title, setTitle] = useState('')
    const [desc, setDesc] = useState('')
    
    const [isVisible, setIsVisible] = useState(false)
    const [type, setType] = useState('back')
    const [photo, setPhoto] = useState(null)

    const toogleCamera = () => {
        setType(type == 'back' ? 'front' : 'back')
    }

    const takePicture = async () => {
        const options = { quality: 0.5, base64: true }
        if (camera) {
            const data = await camera.current.takePictureAsync(options)
            console.log("editProfile -> dataPhoto ", data)
            setPhoto(data)
            setIsVisible(false)
        }
    }

    useEffect(() => {
        const getToken = async () => {
            try {
                const token = await AsyncStorage.getItem("token")
                if (token != null) {
                    return setToken(token)
                }
            } catch (err) {
                console.log(err)
            }
        }
        getToken()
    }, [])

    const onCreatePress = () => {
        const nominal = moneyField.current.getRawValue()
        console.log(nominal)
        const formData = new FormData()
        formData.append('title', title),
        formData.append('description', desc),
        formData.append('donation', nominal),
        formData.append('photo', {
            uri: photo.uri,
            name: 'photo.jpg',
            type: 'image/jpg'
        })
        console.log(formData)
        Axios.post(`${api}/api/donasi/tambah-donasi`, formData, {
            timeout:20000,
            headers : {
                'Authorization' : 'Bearer' + token,
                Accept: 'application/json',
                'Content-Type' : 'multipart/form-data'
            }    
        })
        .then((res)=> {
            console.log("createDonation -> res", res)
            alert("Donasi Baru berhasil ditambahkan")
            navigation.reset({
                index: 0,
                routes: [{name: 'Home'}]
            })
        })
        .catch((err)=> {
            console.log("createDonation -> err", err)
            alert("Create Donation Failed")
        })
    }
    
    const renderCamera = () => {
        return (
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{ flex: 1 }}>
                    <RNCamera
                        style={{ flex: 1 }}
                        type={type}
                        ref={camera}
                    >
                        <View style={styles.camFlipContainer}>
                            <TouchableOpacity style={styles.btnFlip} onPress={() => toogleCamera()}>
                                <Ionicon name="camera-reverse-outline" size={35} />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.btnTakeContainer}>
                            <TouchableOpacity style={styles.btnTake} onPress={() => takePicture()}>
                                <Ionicon name="camera-outline" size={50} />
                            </TouchableOpacity>
                        </View>
                    </RNCamera>
                </View>
            </Modal>
        )
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
            <View>
                <View style={styles.itemInPhoto}>
                    <TouchableOpacity onPress={() => setIsVisible(true)}>
                        <Image source={photo == null ? require('../../../assets/images/photo-blank-grey.png') : { uri: photo.uri }} style={styles.editPhoto} />
                        <View style={styles.btnOpenCamera}>
                            <Ionicon name="camera-outline" size={35} color={colors.black} style={styles.icon} />
                            <Text style={styles.textBtn}>Pilih Gambar</Text>
                        </View>

                    </TouchableOpacity>
                </View>
            </View>
            <View>
                <View style={styles.detailContainer}>
                    <Text>Judul</Text>
                    <TextInput
                        value={title}
                        placeholder="Judul"
                        underlineColorAndroid={colors.grey}
                        onChangeText={(valueTitle)=> setTitle(valueTitle)}
                    />

                    <Text>Deskripsi</Text>
                    <TextInput
                        value={desc}
                        style={styles.textInput}
                        placeholder="Deskripsi"
                        underlineColorAndroid={colors.grey}
                        onChangeText={(valueDesc)=> setDesc(valueDesc) }
                    />

                    <Text>Dana yang dibutuhkan</Text>
                    <TextInputMask
                        type={'money'}
                        options={{
                            precision: 0,
                            separator: ',',
                            delimiter: '.',
                            unit: 'RP. ',
                            suffixUnit: ''
                        }}
                        value={total}
                        ref={moneyField}
                        placeholder='RP. 0'
                        placeholderTextColor={colors.black}
                        underlineColorAndroid={colors.grey}
                        onChangeText={(value) => setTotal(value)}
                    />

                    <TouchableOpacity style={styles.btnLanjut} onPress={() => onCreatePress()}>
                        <Text style={styles.textBtnLanjut}>BUAT</Text>
                    </TouchableOpacity>
                </View>
            </View>
            {renderCamera()}
        </View>
    )
}

export default Bantu;