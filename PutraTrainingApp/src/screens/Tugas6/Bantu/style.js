import { StyleSheet, } from 'react-native'

import colors from '../../../style/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:colors.white
    },
    itemInPhoto:{
        backgroundColor:colors.lightGrey,
        width:"100%",
        height:200,
        justifyContent:'center'  
    },
    icon:{
        alignSelf:'center',
        justifyContent:'center'  
    },
    textBtn:{
        color:colors.black,
        textAlign:'center'
    },
    detailContainer:{
        padding:15
    },
    textInput:{
        paddingBottom:80
    },
    btnLanjut:{
        padding:18,
        backgroundColor:colors.blue,
        borderRadius:10,
        marginTop:10,
    },
    textBtnLanjut:{
        textAlign:'center',
        color:colors.white,
        fontSize:18,
        fontWeight:'bold'
    },
    btnOpenCamera:{
        position: 'absolute',
        width: 50,
        height: 50,
        marginTop:"15%",
        alignSelf: 'center',
        opacity:0.8
    },
    btnFlip:{
        backgroundColor:colors.grey,
        borderRadius:40,
        top:15,
        left:15,
        width:50,
        height:50,
        justifyContent:'center',
        alignItems:'center'
    },

    btnTakeContainer:{
        marginTop:"120%",
        alignSelf:'center'
    },
    btnTake:{
        backgroundColor:colors.grey,
        borderRadius:45,
        width:90,
        height:90,
        justifyContent:'center',
        alignItems:'center'
    },
    editPhoto: {
        height: 200,
        width: "100%",
    },
})

export default styles;