import React, { useEffect, useState } from 'react'
import { View, Text, Image, StatusBar, TouchableOpacity } from 'react-native'
import styles from './style'
import Axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import api from '../../../api'
import { GiftedChat } from "react-native-gifted-chat";
import database from '@react-native-firebase/database';
import colors from '../../../style/colors';

const Inbox = () => {

    const [user, setUser] = useState({})
    const [messages, setMessages] = useState([]);

    useEffect(() => {
        const getToken = async () => {
            try {
                const token = await AsyncStorage.getItem("token")
                if (token != null) {
                    // console.log("getToken -> token", token)
                    return getProfile(token)
                }
            } catch (err) {
                console.log(err)
            }
        }
        getToken()
        getProfile()
        onRef()

        return () => {
            const db = database().ref('messages')
            if(db){
                db.off()
            }
        }
    }, [])

    const getProfile = (token) => {
        Axios.get(`${api}/api/profile/get-profile`, {
            timeout: 20000,
            headers: {
                "Authorization": "Bearer" + token,
                'Accept' : 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then((res) => {
                const data = res.data.data.profile
                setUser(data)
                console.log("data didapatkan -> ", res.data.data.profile)
            })
            .catch((err) => {
                console.log("Profile -> err", err)
            })
    }

    const onRef = ()=> {
        database().ref('messages').limitToLast(20).on('child_added', snapshot => {
            const value = snapshot.val()
            setMessages(previousMessages => GiftedChat.append(previousMessages, value))
        })
    }

    const onSend =((messages = [])=>{
        for(let i = 0; i < messages.length; i++){
            database().ref('messages').push({
                _id: messages[i]._id,
                createdAt: database.ServerValue.TIMESTAMP,
                text: messages[i].text,
                user: messages[i].user
            })
        }
    })

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
            <View style={styles.containerHeader}>
                <View style={styles.textLogoContainer}>
                    <Text style={styles.textLogo}>Inbox</Text>
                </View>
            </View>
            <GiftedChat 
                messages={messages}
                onSend={(messages) => onSend(messages)}
                user={{
                    _id: user.id,
                    name: user.name,
                    avatar: `${api}${user.photo}`
                }}
                showUserAvatar
            />
        </View>
    )
}

export default Inbox;