import { StyleSheet, } from 'react-native'

import colors from '../../../style/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:colors.white
    },
    containerHeader:{
        justifyContent:'center',
        height:50,
        backgroundColor:colors.blue
    },
    textLogo: {
        fontSize: 18,
        fontWeight: 'bold',
        color: colors.white,
    },
    textLogoContainer:{
        marginLeft:20,
    },
})

export default styles;