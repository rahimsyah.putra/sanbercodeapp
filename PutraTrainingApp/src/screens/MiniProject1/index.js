import React, { useState, createContext } from 'react'


import Contact from './Contact'

export const RootContext = createContext();

const context = () => {
    const [inputName, setName] = useState('')
    const [inputNumber, setNumber] = useState('')
    const [contact, setContact] = useState([])

    const Provider = RootContext.Provider

    handleChangeInputName = (name) => {
        setName(name)

    }

    handleChangeInputNumber = (number) => {
        setNumber(number)
    }

    addNewContact = () => {
        setContact([...contact, { name: inputName, number: inputNumber }])
        setName('')
        setNumber('')
    }

    removeContact = (id) => {
        setContact(
            contact.filter((contac, index) => {
                if (index !== id) {
                    return true
                }
            })
        )
    }

    return (
        <Provider value={{
            inputName,
            inputNumber,
            contact,
            handleChangeInputName,
            handleChangeInputNumber,
            addNewContact,
            removeContact
        }}>
            <Contact/>
        </Provider>
    )
}

export default context;