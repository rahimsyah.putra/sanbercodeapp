import React, { useContext } from 'react'
import {
    View,
    Text,
    StyleSheet,
    StatusBar,
    TouchableOpacity,
    TextInput,
    FlatList,
    SafeAreaView,
    ScrollView,
} from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';

import { RootContext } from '../MiniProject1'

const Contact = () => {
    return (
        <ScrollView style={styles.container}>
            <StatusBar barStyle="light-content" backgroundColor='#3498db' />
            <View >
                <TabBar />
                <TabForm />
            </View>
        </ScrollView>
    )
}

const TabBar = () => {
    return (
        <View style={styles.tabBar}>
            <Text style={styles.textBar}>
                Contact
            </Text>
        </View>
    )
}

const TabForm = () => {

    const Consumer = RootContext.Consumer

    return (
        <Consumer>
            {
                state => (
                    <View>
                        <View style={styles.containerForm}>
                            <View>
                                <Text style={styles.textJudul}>
                                    Tambah Kontak
                                </Text>
                                <Icon name="person-circle-sharp" style={styles.photo} size={120} />
                                <View style={styles.tabInput}>

                                    <View style={styles.formInput}>
                                        <TextInput
                                            style={styles.inputText}
                                            placeholder="Name"
                                            value={state.inputName}
                                            onChangeText={(name) => state.handleChangeInputName(name)}
                                        />
                                    </View>
                                </View>
                                <View style={styles.tabInput}>

                                    <View style={styles.formInput}>
                                        <TextInput
                                            style={styles.inputText}
                                            placeholder="Phone Number"
                                            value={state.inputNumber}
                                            onChangeText={(number) => state.handleChangeInputNumber(number)}
                                        />
                                    </View>
                                </View>

                                <TouchableOpacity onPress={() => state.addNewContact(state.inputName, state.inputNumber)}>
                                    <Icon style={styles.btn} name="add-outline" size={25} />
                                </TouchableOpacity>
                            </View>

                        </View>
                        <FlatList
                            horizontal
                            data={state.contact}
                            keyExtractor={item => item.name}
                            renderItem={({ item, index }) =>
                                <View style={styles.containerContact}>
                                    <View style={styles.positionContact}>
                                        <Icon name="person-circle-sharp" style={styles.photo} size={120}/>
                                        <Text style={styles.textName}>
                                            {item.name}
                                        </Text>
                                        <Text style={styles.textNum}>
                                            {item.number}
                                        </Text>
                                        <TouchableOpacity onPress={() => state.removeContact(index)}>
                                        <Icon style={styles.btnRemove} name="trash-outline" size={25} />
                                    </TouchableOpacity>
                                    </View>
                                    
                                </View>
                            }
                        />
                    </View>
                )
            }
        </Consumer>
    )
}

export default Contact;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f5f5f5',
    },
    tabBar: {
        backgroundColor: '#3498db',
        padding: 15,
        marginBottom: 8
    },
    textBar: {
        alignSelf: 'center',
        fontWeight: 'bold',
        color: '#ffffff',
        fontSize: 20
    },
    containerForm: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        padding: 2,
        backgroundColor: '#f5f5f5'
    },
    containerContact: {
        flexDirection: 'row',
        marginHorizontal: 8,
        marginVertical: 5,
        maxHeight:330,
        maxWidth:250,
        padding: 2,
        paddingVertical: 13,
        borderWidth: 2,
        borderRadius: 20,
        borderColor: '#e0e0e0',
        elevation: 5,
        backgroundColor: '#ffffff'
    },
    positionContact: {
        flexDirection: 'column',
        marginHorizontal: 10,
        backgroundColor: '#ffffff',
        maxWidth:150,
        justifyContent:'space-between'
    },
    tabInput: {
        flexDirection: 'row',
        marginBottom: 10
    },
    formInput: {
        flex: 1,
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#3498db',
        marginLeft: 8,
        marginRight: 8,
        backgroundColor: '#ffffff'
    },
    inputText: {
        marginLeft: 4,
        fontSize: 16
    },
    textName: {
        marginBottom: 5,
        fontSize: 20,
        fontWeight: 'bold',
        color: '#000000',
        textAlign:'center'
    },
    textNum: {
        marginBottom: 5,
        fontSize: 16,
        color: '#000000',
        textAlign:'center'
    },
    textJudul: {
        marginBottom: 10,
        fontSize: 16,
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    photo: {
        alignSelf: 'center',
        color: '#3498db',
        marginLeft: 5
    },
    btn: {
        backgroundColor: '#3498db',
        borderRadius: 10,
        padding: 12,
        alignSelf: 'center',
        marginLeft: 5,
        marginRight: 8
    },
    btnRemove: {
        backgroundColor: '#ffffff',
        borderRadius: 10,
        padding: 12,
        alignSelf:'center'
    }
});
