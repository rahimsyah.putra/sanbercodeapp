import React from 'react'
import { View, Text, StyleSheet, StatusBar, Image } from 'react-native'

import Ionicon from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/MaterialIcons';

const tugas2 = () => {
    return (
        <>
            <StatusBar barStyle="light-content" backgroundColor='#3498db' />
            <View style={styles.container}>
                <TabBar />
                <Profile />
                <Setting />

            </View>
        </>
    )
}

const TabBar = () => {
    return (
        <View style={styles.tabBar}>
            <Text style={styles.textBar}>
                Account
                </Text>
        </View>
    )
}

const Profile = () => {
    return (
        <View >
            <View style={styles.profileBar}>
                <View style={styles.pembatas}>
                    <View style={styles.posisi}>
                        <Image source={{ uri: 'https://variety.com/wp-content/uploads/2015/07/naruto_movie-lionsgate.jpg?resize=450,253' }} style={styles.photo} />
                        <Text style={styles.textNama}>
                            Muhammad Rahimsyah Putra
                    </Text>
                    </View>
                </View>
                <View style={styles.posisi}>
                    <Ionicon style={styles.item} name="wallet-outline" size={25} />
                    <Text style={styles.text}>
                        Saldo
                    </Text>
                    <Text style={styles.textSaldo}>
                        Rp. 120.000.000
                    </Text>
                </View>
            </View>

        </View>
    )
}

const Setting = () => {
    return (
        <View>
            <View View style={styles.settingBar}>
                <View style={styles.pembatas}>
                    <View style={styles.posisi}>
                        <Icon style={styles.item} name="settings" size={25} />
                        <Text style={styles.text}>
                            Pengaturan
                        </Text>
                    </View>
                </View>
                <View style={styles.pembatas}>
                    <View style={styles.posisi} >
                        <Icon style={styles.item} name="help-outline" size={25} />
                        <Text style={styles.text}>
                            Bantuan
                        </Text>
                    </View>

                </View>
                <View style={styles.posisi}>
                    <Ionicon style={styles.item} name="receipt-outline" size={25} />
                    <Text style={styles.text}>
                        Syarat & Ketentuan
                        </Text>
                </View>
            </View>
            <View View style={styles.outBar}>
                <View style={styles.posisi}>
                    <Ionicon style={styles.item} name="log-out-outline" size={25} />
                    <Text style={styles.text}>
                        Keluar
                    </Text>
                </View>
            </View>
        </View>
    )
}
export default tugas2

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#bdc3c7',
    },
    tabBar: {
        backgroundColor: '#3498db',
        padding: 18,

    },
    textBar: {
        fontWeight: 'bold',
        color: '#ffffff',
        fontSize: 20
    },
    profileBar: {
        flexDirection: 'column',
        backgroundColor: '#ffffff',
        padding: 5,
        borderBottomColor: '#bdc3c7',
        borderBottomWidth: 5,
    },
    outBar: {
        backgroundColor: '#ffffff',
        borderBottomColor: '#bdc3c7',
        borderBottomWidth: 5,
        padding: 5,
    },
    pembatas: {
        borderBottomColor: '#bdc3c7',
        borderBottomWidth: 1,
    },
    settingBar: {
        flexDirection: 'column',
        backgroundColor: '#ffffff',
        padding: 5,
        borderBottomColor: '#a6a6a6',
        borderBottomWidth: 5,
    },
    posisi: {
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        padding: 20,
    },
    textNama: {
        fontWeight: 'bold',
        marginLeft: 10,
        alignSelf: 'center'
    },
    photo: {
        height: 50,
        width: 50,
        borderRadius: 20,
    },
    text: {
        fontSize: 15,
        alignSelf: 'center'
    },
    textSaldo: {
        marginLeft: 150,
        alignSelf: 'center'
    },
    item: {
        marginRight: 10
    }
})