import React, { useEffect } from 'react';
import Tugas6 from './src/navigation/routes'
import firebase from '@react-native-firebase/app';
import OneSignal from 'react-native-onesignal';
import codePush from 'react-native-code-push';

//import Tugas from './src/screens/Tugas1/Tugas1'
//import Tugas2 from './src/screens/Tugas2/index'
//import Tugas3 from './src/screens/Tugas3/TodoList'
//import Tugas4 from './src/screens/Tugas4/index'

//import MiniProject1 from './src/screens/MiniProject1/index'

//ganti isi dari firebaseConfig, sesuai dengan project yang anda buat di firebase
var firebaseConfig = {
  apiKey: "AIzaSyCbPM7Zq7yKld_AD7qvNyum4hKeojo21wA",
  authDomain: "sanbercode-ef24c.firebaseapp.com",
  databaseURL: "https://sanbercode-ef24c-default-rtdb.firebaseio.com",
  projectId: "sanbercode-ef24c",
  storageBucket: "sanbercode-ef24c.appspot.com",
  messagingSenderId: "398038654110",
  appId: "1:398038654110:web:ce73112bc7affd768eaf8f",
  measurementId: "G-MG3LSVRSB2"
};
// Inisialisasi firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)

}

const App = () => {
  useEffect(() => {
    OneSignal.setLogLevel(6, 0);

    OneSignal.init("98640335-a17b-4321-8305-bb90b1da99ce", { kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false, kOSSettingsKeyInFocusDisplayOption: 2 });
    OneSignal.inFocusDisplaying(2);

    codePush.sync({
      updateDialog: true,
      installMode: codePush.InstallMode.IMMEDIATE
    }, SyncStatus)
  }, [])
  
  const SyncStatus = (status)=>{
    switch(status){
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        console.log("Checking for Update")
        break;
      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        console.log("Downloading Package")
        break;
      case codePush.SyncStatus.UP_TO_DATE:
        console.log("Up to date")
        break;
      case codePush.SyncStatus.INSTALLING_UPDATE:
        console.log("Installing update")
        break;
      case codePush.SyncStatus.UPDATE_INSTALLED:
        Alert.alert("Notification", "Update Installed")
        break;
      case codePush.SyncStatus.AWAITING_USER_ACTION:
        console.log("Awaiting user")
      default:
        break;
    }
  }

  return (
    <>
      <Tugas6 />
    </>
  );
};

export default App;